<?php

namespace App\Http\Controllers\Vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VendorService extends Controller
{
    //
	public function __construct()
	{
		$this->data['url_imoidmarco']               = config('url_api.url_idmarco');
		$this->data['url_imoidmarco_client_id']     = config('url_api.client_id_idmarco');
		$this->data['url_imoidmarco_client_secret'] = config('url_api.client_secret_idmarco');
	}

    public function gettokenimo($value='')
    {
        # code...
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $this->data['url_imoidmarco']."/api/v1/access_token",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "grant_type=client_credentials&client_id=".$this->data['url_imoidmarco_client_id']."&client_secret=".$this->data['url_imoidmarco_client_secret'],
          CURLOPT_HTTPHEADER => array(
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: caae8e83-12b7-4b2a-b50b-1b2b09679b78"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
            return json_decode($response);
          // echo $response;
        }
    }

    public function getstockpoint($value='')
    {
        # code...
        $gettoken = $this->gettokenimo();
        $lastsync = \App\Models\VendorZone::orderBy("lastsyncvendor")->count() > 0 ? \Carbon\Carbon::parse(\App\Models\VendorZone::orderBy("lastsyncvendor", "desc")->first()->lastsyncvendor) : "0000-00-00 00:00:00";

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $this->data['url_imoidmarco']."/api/v1/pw/get_stockpoint",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "updated_at=".urlencode($lastsync),
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer $gettoken->access_token",
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 2af9b399-f8ed-43e4-a640-f6012dcb4053"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {          
          return json_decode($response);
        }
    }

    public function getproduct($updated_at='0000-00-00 00:00:00')
    {
        # code...
        $gettoken = $this->gettokenimo();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $this->data['url_imoidmarco']."/api/v1/pw/get_product",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "updated_at=".$updated_at,
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer $gettoken->access_token",
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 2af9b399-f8ed-43e4-a640-f6012dcb4053"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {          
          return json_decode($response);
        }
    }

    public function getproductinactive($updated_at='0000-00-00 00:00:00')
    {
        # code...
        $gettoken = $this->gettokenimo();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $this->data['url_imoidmarco']."/api/v1/pw/get_product_inactive",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "updated_at=".$updated_at,
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer $gettoken->access_token",
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 2af9b399-f8ed-43e4-a640-f6012dcb4053"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {          
          return json_decode($response);
        }
    }

    public function getprice($updated_at='0000-00-00 00:00:00')
    {
        # code...
        $gettoken = $this->gettokenimo();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $this->data['url_imoidmarco']."/api/v1/pw/get_price",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "updated_at=".$updated_at,
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer $gettoken->access_token",
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 2af9b399-f8ed-43e4-a640-f6012dcb4053"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {          
          return json_decode($response);
        }
    }

    public function getstock($updated_at='0000-00-00 00:00:00')
    {
        # code...
        $gettoken = $this->gettokenimo();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $this->data['url_imoidmarco']."/api/v1/pw/get_stock",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "updated_at=".$updated_at,
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer $gettoken->access_token",
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 2af9b399-f8ed-43e4-a640-f6012dcb4053"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {          
          return json_decode($response);
        }
    }

    public function postcustomer($value='')
    {
        # code...
        $gettoken = $this->gettokenimo();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $this->data['url_imoidmarco']."/api/v1/pw/post_customer",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "data=".json_encode($value),
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer $gettoken->access_token",
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 2af9b399-f8ed-43e4-a640-f6012dcb4053"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          return json_decode($response);
        }

    }

    public function postorder($value='')
    {
        # code...
        $gettoken = $this->gettokenimo();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $this->data['url_imoidmarco']."/api/v1/pw/post_order_paid",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "data=".json_encode($value),
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer $gettoken->access_token",
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 2af9b399-f8ed-43e4-a640-f6012dcb4053"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          return json_decode($response);
        }

    }
    
    public function getstockpointcoverage($value='')
    {
        # code...
        $gettoken = $this->gettokenimo();
        $lastsync = \App\Models\VendorZoneCoverage::orderBy("lastsyncvendor")->count() > 0 ? \Carbon\Carbon::parse(\App\Models\VendorZoneCoverage::orderBy("lastsyncvendor", "desc")->first()->lastsyncvendor) : "0000-00-00 00:00:00";

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $this->data['url_imoidmarco']."/api/v1/pw/get_stockpoint_coverage",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "updated_at=".urlencode($lastsync),
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer $gettoken->access_token",
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 2af9b399-f8ed-43e4-a640-f6012dcb4053"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          return json_decode($response);
        }
    }

    public function getdatamwstatus($updated_at='0000-00-00 00:00:00')
    {
        # code...
        $gettoken = $this->gettokenimo();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $this->data['url_imoidmarco']."/api/v1/pw/get_paid_order",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "updated_at=".$updated_at,
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer $gettoken->access_token",
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 2af9b399-f8ed-43e4-a640-f6012dcb4053"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          return json_decode($response);
        }
    }

    public function getorderstatus($updated_at='0000-00-00 00:00:00')
    {
        # code...
        $gettoken = $this->gettokenimo();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $this->data['url_imoidmarco']."/api/v1/pw/get_paidsalesorderdelv",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "updated_at=".$updated_at,
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer $gettoken->access_token",
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 2af9b399-f8ed-43e4-a640-f6012dcb4053"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          return json_decode($response);
        }
    }

    public function getordertracking($updated_at='0000-00-00 00:00:00')
    {
        # code...
        $gettoken = $this->gettokenimo();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $this->data['url_imoidmarco']."/api/v1/pw/get_paid_order_tracing",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "updated_at=".$updated_at,
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer $gettoken->access_token",
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 2af9b399-f8ed-43e4-a640-f6012dcb4053"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          return json_decode($response);
        }
    }

    public function postrevision($value='')
    {
        # code...
        $gettoken = $this->gettokenimo();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $this->data['url_imoidmarco']."/api/v1/pw/get_order_revision",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "data=".json_encode($value),
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer $gettoken->access_token",
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 2af9b399-f8ed-43e4-a640-f6012dcb4053"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          return json_decode($response);
        }

    }


    public function post_orderresuffle($value='')
    {
        $gettoken = $this->gettokenimo();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $this->data['url_imoidmarco']."/api/v1/pw/post_reshuffle",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "data=".json_encode($value),
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer $gettoken->access_token",
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 2af9b399-f8ed-43e4-a640-f6012dcb4053"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          return json_decode($response);
        }
    }
    public function update_orderresuffle($value='')
    {
        $gettoken = $this->gettokenimo();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $this->data['url_imoidmarco']."/api/v1/pw/update_reshuffle",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "data=".json_encode($value),
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer $gettoken->access_token",
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 2af9b399-f8ed-43e4-a640-f6012dcb4053"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          return json_decode($response);
        }
    }
}
