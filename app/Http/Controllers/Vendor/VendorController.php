<?php

namespace App\Http\Controllers\Vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Vendor;
use Freshbitsweb\Laratables\Laratables;
use Redirect;
use Illuminate\Support\Facades\Input;

class VendorController extends Controller
{
    public function index(){
    	$vendors = Vendor::orderBy('id','desc')->get();
    	// return $vendors;
    	return view('vendor.index', ['vendors' => $vendors]);
    }

    public function ajaxvendor(){
    	return Laratables::recordsOf(Vendor::class);
    }

    public function create(){
    	return view('vendor.form.create');
    }

    public function update($id){
    	$id = Vendor::find($id);
    	// $create = $request->
    	return view('vendor.form.update', ['id' => $id]);
    }

    public function simpan($id){
    	$store = Vendor::find($id);
    	$store->server_url	 = Input::get('server_url');
    	$store->server_username	 = Input::get('server_username');
    	$store->server_password	 = Input::get('server_password');
    	$store->save();

    	// return 'sukses'	;
    	 return redirect('master/vendor')->with('update', 'Sukses Mengubah Data!');
    }


}
