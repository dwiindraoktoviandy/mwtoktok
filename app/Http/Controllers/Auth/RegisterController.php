<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data, $user='')
    {
        $input = $data;

        if (isset($data['_method'])) {
            

            if ($data['username'] == $user->username) {
                $username ='required';
            } else {
                $username = 'required|string|min:6|max25|unique:users';
            }

            if ($data['email'] == $user->email) {
                $email == 'required';
            } else {
                $email = 'required|string|email|max:255|unique:users';
            }

            if ($data['password'] != '' ) {
                $password = 'required|string|min:6|confirmed';
            } else {
                $password = '';
            }


        } else {

        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'username' => 'required|string|min:6',
            'usergroup_id' => 'required',
        ]);

        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {   
        if (isset($data['_method'])) {
            $user->name     = $data['name'];
            $user->username     = $data['username'];
            $user->email     = $data['email'];
            $user->usergroup_id     = $data['usergroup_id'];

            if ($data['password'] != '') {
                $user->password = bcrypt($data['password']);
            }

            $user->update();

        } else {

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'username' => $data['username'],
            // 'password' => Hash::make($data['password']),
            'password' => bcrypt($data['password']),
            'usergroup_id' => $data['usergroup_id'],
        ]);

        }
    }
}
