<?php

namespace App\Http\Controllers\Order;

use DataTables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;

class OrderController extends Controller
{
    public function indextoktok(Request $request)
    {
    	if($request->ajax())
    	{
    		$order = Order::select([
    			'no_order',
    			'order_date',
    			'payment_type',
    			'status_order',
    			'lastsynctoktok',
    			'lastsyncvendor'
    		]);

    		return DataTables::eloquent($order)->toJson();
    	}
    	return view('order.ordertoktok', compact('order'));
    }
}
