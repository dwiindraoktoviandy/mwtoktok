<?php

namespace App\Http\Controllers\Price;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Price;
use DataTables;

class PriceController extends Controller
{
    public function index(Request $request)
    {
    	if($request->ajax())
    	{
	    	$prices = Price::query();		
	    	return DataTables::eloquent($prices)
	    		->editColumn('vendor_price', function(Price $price) {
	    			return number_format($price->vendor_price, 2, ',', '.');
	    		})
	    		->toJson();
    	}
    	return view('price.index');
    }
}
