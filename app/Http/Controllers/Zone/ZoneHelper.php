<?php

namespace App\Http\Controllers\Zone;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ZoneHelper extends Controller
{
    //
    public function getstockpointvendor($value='')
    {
    	# code...
    	$service       = new \App\Http\Controllers\Vendor\VendorService();
    	$getstockpoint = $service->getstockpoint();
    	$success = 0;

    	if(count($getstockpoint) > 0){
    		foreach ($getstockpoint->data as $key => $value) {
    			# code...
    			$store = \App\Models\VendorZone::updateOrCreate(
							[
								'kode_sp' => $value->kode_sp
							],
							[
								'nama_zona' => $value->nama_stockpoint,
								'kode_sp' => $value->kode_sp,
								// 'toktok_zone_ids' => $value->,
								'provinsi' => $value->provinsi,
								'kota' => $value->kota,
								'kecamatan' => $value->kecamatan,
								'kelurahan' => $value->kelurahan,
								'no_telepon' => $value->no_telepon,
								'no_fax' => $value->no_fax,
								// 'status' => $value->status,
								'bacd' => $value->bacd,
								'pic_nama' => $value->spo_nama,
								'pic_hp' => $value->spo_hp,
								'pic_email' => $value->spo_email,
								'alamat' => $value->alamat,
								'lastsyncvendor' => \Carbon\Carbon::parse($value->updated_at),
							]
						);

    			$success++;
    		}
    	}

    	return [
    		"status" => true,
    		"count" => $success,
    		"messages" => "Success",
    	];
    }
    public function getstockpointcoveragevendor($value='')
    {
    	# code...
    	$service       = new \App\Http\Controllers\Vendor\VendorService();
    	$getstockpoint = $service->getstockpointcoverage();
    	$success = 0;

    	if(count($getstockpoint) > 0){
    		foreach ($getstockpoint->data as $key => $value) {
    			# code...
    			$store = \App\Models\VendorZoneCoverage::updateOrCreate(
							[
								'provinsi' => $value->provinsi,
								'kota' => $value->kota,
								'kecamatan' => $value->kecamatan,
								'kelurahan' => $value->kelurahan,
							],
							[
								'zone_id' =>0,
								'kelurahan_id' =>0,
								'kelurahan_code' =>$value->kelurahan_code,
					            'provinsi' =>$value->provinsi,
					            'kota' =>$value->kota,
					            'kecamatan' =>$value->kecamatan,
					            'kelurahan' =>$value->kelurahan,
					            'kodepos' =>$value->kodepos,
					            'createdby' =>"cronjob",
					            'modifiedby' =>"cronjob",
					            'kode_sp' =>$value->kode_sp,
					            'lastsyncvendor' =>$value->updated_at,
							]
						);

    			$success++;
    		}
    	}

    	return [
    		"status" => true,
    		"count" => $success,
    		"messages" => "Success",
    	];
    }
}
