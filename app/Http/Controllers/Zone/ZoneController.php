<?php

namespace App\Http\Controllers\Zone;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Freshbitsweb\Laratables\Laratables;

class ZoneController extends Controller
{
    //
    public function getstockpoint($value='')
    {
    	# code...
    	return Laratables::recordsOf(\App\Models\VendorZone::class);
    }
    public function index($value='')
    {
    	# code...
    	$this->data['data']	= \App\Models\VendorZone::get();
    	return view('zone.index')->with($this->data);
    }
    public function getstokcpointvendor($value='')
    {
    	# code...
    	$helper = new \App\Http\Controllers\Zone\ZoneHelper;
    	return $helper->getstockpointvendor();
    }
    public function getstockpointcoveragevendor($value='')
    {
    	# code...
    	$helper = new \App\Http\Controllers\Zone\ZoneHelper;
    	return $helper->getstockpointcoveragevendor();
    }
    public function btngetstockpointvendor($value='')
    {
    	# code...
    	$run = $this->getstokcpointvendor();
    	return redirect()->route('transaction.zone.index');
    }
    public function btngetstockpointcoveragevendor($value='')
    {
    	# code...
    	$run = $this->getstockpointcoveragevendor();
    	
    	return redirect()->route('transaction.zone.index');
    }
}
