<?php

namespace App\Http\Controllers\Inventory;

use DataTables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Stock;

class InventoryController extends Controller
{
    public function index(Request $request)
    {
    	if($request->ajax())
    	{
	    	$stocks = Stock::query();		
	    	return DataTables::eloquent($stocks)
	    		->toJson();
    	}
        return view('inventory.index');
    }
}
