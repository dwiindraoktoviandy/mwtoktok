<?php

namespace App\Http\Controllers\Api\Imo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Exception;
use Redirect;
use Log;

use App\Models\MerchantSetup;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderStatus;
use App\Models\Price;
use App\Models\Product;
use App\Models\ProductInactive;
use App\Models\Stock;
use App\Models\Vendor;
use App\Models\Customer;
use App\Models\Paidsorevreq;
use App\Models\Paidsorevresult;
use App\Models\Paidsorevapproval;
use App\Models\OrderResuffle;
use App\Models\OrderResuffleDetail;

class ImoHelper extends Controller
{

    public static function setdataproduct($value='')
    {
        # code...
        return [];
    }    

    public static function setdatavendor($getdatavendorimo)
    {
          // return json_decode($getdatavendorimo)->data;
      // return $data = json_decode($getdatavendorimo)->data;
        // $i = '';
         foreach ($data = json_decode($getdatavendorimo)->data as $key) {
                try {
                    \DB::beginTransaction();
                  $vendors = str_replace('"', '', json_encode($key->nama_vendor));
                  $vendorsid = str_replace('"', '', json_encode($key->id));
                  // $vendors = json_encode([$key->nama_vendor]);

                   // $savevendor = Vendor::where('vendor_name', $vendors)->count();
                   // $count = Vendor::where('vendor_name', $vendors)->get();
               
                    $update = Vendor::updateOrCreate(
                        ['vendor_id' => $vendorsid],
                        ['vendor_name' => $vendors]
                    );
                    // dd($update);
                    // if (count($update) == 0 ) {
                    //     return Redirect::to('master/vendor')->with('kosong','Succes Get Vendor');
                    // }
                   // if (empty($savevendor)) {
                   //     $savevendor = Vendor::Create([
                   //      'vendor_name' => $vendors
                   //     ]);
                   // }else{
                   //      $savevendor->update([
                   //          'vendor_name' => $vendors]);
                   //  // return 'ga ada';
                   // }
                    // $a = json_encode($value->nama_vendor);
                    // $i.=$a;
                     // $1++;
                 \DB::commit(); 

                } catch (Exception $e) {
                    \Log::error(json_encode([$getdatavendorimo]));
                }
            }

      return Redirect::to('master/vendor')->with('status','Succes Get Vendor');
        // \Log::error(json_encode([$getdatavendorimo]));
    }    


    public static function setdatacustomer($getdatacustomer)
    {
          // return json_decode($getdatavendorimo)->data;
      // return $data = json_decode($getdatavendorimo)->data;
        // $i = '';
           foreach ($data = json_decode($getdatacustomer)->data as $key) {
                try {
                    \DB::beginTransaction();
                    // return $key->fe_id;
                  $vendors = str_replace('"', '', json_encode($key->fe_id));
                  $vendorsid = str_replace('"', '', json_encode($key->nip));
                  $nama = str_replace('"', '', json_encode($key->nama));
                  $alamat = str_replace('"', '', json_encode($key->alamat));
                  if ($vendors == 'null') {
                    $vendors = 0;
                  }
                  if ($vendorsid == 'null') {
                    $vendorsid = 0;
                  }
                  if ($nama == 'null') {
                    $nama = 0;
                  }
                  if ($alamat == 'null') {
                    $alamat = 0;
                  }
                  // $vendors = json_encode([$key->nama_vendor]);

                   // $savevendor = Vendor::where('vendor_name', $vendors)->count();
                   // $count = Vendor::where('vendor_name', $vendors)->get();
               
                    $update = Customer::updateOrCreate(
                        ['fe_id' => $vendorsid],
                        ['nip' => $vendors],
                        ['nama' => $nama],
                        ['alamat' => $alamat]
                    );
                    // dd($update);
                    // if (count($update) == 0 ) {
                    //     return Redirect::to('master/vendor')->with('kosong','Succes Get Vendor');
                    // }
                   // if (empty($savevendor)) {
                   //     $savevendor = Vendor::Create([
                   //      'vendor_name' => $vendors
                   //     ]);
                   // }else{
                   //      $savevendor->update([
                   //          'vendor_name' => $vendors]);
                   //  // return 'ga ada';
                   // }
                    // $a = json_encode($value->nama_vendor);
                    // $i.=$a;
                     // $1++;
                 \DB::commit(); 

                } catch (Exception $e) {
                    \Log::error(json_encode([$getdatacustomer]));
                }
            }
        return Redirect::to('customer')->with('status','Succes Get Customer');
        // \Log::error(json_encode([$getdatavendorimo]));
    }

    // data customer
    public static function setdatacustomerbaru($customer='')
    {
        $total = $customer->total;
        $created = 0;
        $updated = 0;
        $not_updated = 0;
        $data = $customer->data;
        foreach($data as $row)
        {
            $count = Customer::where('fe_id', $row->customerfrontend_id)->count();
            if($count > 0)
            {
                // $temp_cust = Customer::where('fe_id', $row->customerfrontend_id)->first();
                // if(Carbon::parse($row->created_at) > Carbon::parse($temp_cust->lastsynctoktok))
                // {
                //     $result = self::updateCustomer($row);
                //     if($result == 1)
                //     {
                //         $updated++;
                //     }
                // }
                // else
                // {
                //     $not_updated++;
                // }
            }
            else
            {
                $result = self::storeCustomer($row);
                if($result == 1)
                {
                    $created++;
                }
            }
        }
        Log::info(json_encode(['message' => 'Get Data Customer From TokTok','tanggal' => Carbon::now()->toDateTimeString(), 'total' => $total, 'created' => $created, 'updated' => $updated, 'not_updated' => $not_updated]));
        return response(['total' => $total, 'created' => $created, 'updated' => $updated, 'not_updated' => $not_updated]);
    }

    // create customer
    public static function storeCustomer($data='')
    {
        $customer = new Customer;
        $customer->fe_id = $data->customerfrontend_id;
        $customer->firstname = $data->owner_outlet_firstname; 
        $customer->lastname = $data->owner_outlet_lastname;
        if(!empty($data->outlet_delivery_address_first))
        {
            $customer->alamat = $data->outlet_delivery_address_first->address;
            $customer->provinsi = strtoupper($data->outlet_delivery_address_first->nama_provinsi);
            $customer->kota = strtoupper($data->outlet_delivery_address_first->nama_kota);
            $customer->kecamatan = strtoupper($data->outlet_delivery_address_first->nama_kecamatan);
            $customer->kelurahan = strtoupper($data->outlet_delivery_address_first->nama_kelurahan);
        }
        $customer->notelp_cust = $data->owner_hp;
        $customer->lastsynctoktok = $data->paid->created_at;
        $customer->save();
        return 1;
    }

    // update customer
    public static function updateCustomer($data='')
    {
        $customer = Customer::where('fe_id', $data->customerfrontend_id)->first();
        $customer->fe_id = $data->customerfrontend_id;
        $customer->nama = $data->owner_outlet_firstname . ' ' . $data->owner_outlet_lastname;
        if(!empty($data->outlet_delivery_address_first))
        {
            $customer->alamat = $data->outlet_delivery_address_first->address;
            $customer->provinsi = strtoupper($data->outlet_delivery_address_first->nama_provinsi);
            $customer->kota = strtoupper($data->outlet_delivery_address_first->nama_kota);
            $customer->kecamatan = strtoupper($data->outlet_delivery_address_first->nama_kecamatan);
            $customer->kelurahan = strtoupper($data->outlet_delivery_address_first->nama_kelurahan);
        }
        $customer->notelp_cust = $data->owner_hp;
        $customer->lastsynctoktok = $data->paid->created_at;
        $customer->update();
        return 1;
    }

    // data paidsalesorder
    public static function storeOrder($data='', $details='')
    {
        $order = Order::create($data);
        foreach($details as $detail)
        {
            if(!empty($detail->vendor))
            {
                if(strtoupper($detail->vendor->kode_vendor) == 'IDM')
                {
                    $order_detail = self::storeOrderDetail($detail, $order->id);
                }
            }
        }
        return ['result' => 1];
    }

    public static function updateOrder($data='', $details='', $order_id)
    {
        $order = Order::find($order_id);
        $data['needsyncvendor'] = 0;
        $order->update($data);
        $order_detail_lama = OrderDetail::where('order_id', $order->id)->get();

        foreach($order_detail_lama as $detail_lama)
        {
            $detail_lama->delete();
        }

        foreach($details as $detail)
        {
            if(!empty($detail->vendor))
            {
                if(strtoupper($detail->vendor->kode_vendor) == 'IDM')
                {
                    self::storeOrderDetail($detail, $order->id);   
                }
            }
        }
        
        return ['result' => 1];
    }

    public static function storeOrderDetail($data='', $order_id='')
    {
        $order_detail = [];

        $order_detail['order_id'] = $order_id;
        $order_detail['product_id'] = $data->product_id;
        $order_detail['product_frontend_id'] = $data->productfrontend_id ? $data->productfrontend_id : 0;
        $order_detail['qty'] = $data->qty;
        $order_detail['price'] = $data->buying_price;
        $order_detail['sku'] = $data->sku;
        $order_detail['nama_fe'] = $data->namafe;
        $order_detail['uom'] = $data->uom;
        $order_detail['remark'] = $data->remark;
        $order_detail['gross_amount'] = $data->buying_price * $data->qty;
        $order_detail['fe_sync_date'] = $data->fesynchdate ? $data->fesynchdate: Carbon::now()->toDateTimeString();
        $order_detail['net_amount'] = $data->buying_price * $data->qty;
        $order_detail['disc_amount'] = 0;
        $order_detail['vat_percent'] = $data->vatpercent ? $data->vatpercent : 0;
        $order_detail['sync_date'] = $data->synchdate ? $data->synchdate : $data->updated_at;
        $order_detail['ori_price'] = $data->buying_price;
        $order_detail['ori_qty'] = $data->ori_qty;
        $order_detail['ori_discount'] = 0;
        $order_detail['conversion'] = $data->conversion;
        $order_detail['last_mw_sync_date'] = $data->lastmwsynchdate;
        $order_detail['last_fe_sync_date'] = $data->lastfesynchdate;
        $result = OrderDetail::create($order_detail);
        return $result;
    }

    public static function olahDataOrder($data_temp='')
    {
        // hitung payment amount
        $paymentamount = 0;
        $discount = 0;
        foreach($data_temp->details as $detail)
        {
            if(!empty($detail->vendor))
            {
                if(strtoupper($detail->vendor->kode_vendor) == 'IDM')
                {
                    $paymentamount += ($detail->buying_price * $detail->qty);
                }
            }
        }
        // $paymentamount -= !empty($data_temp->deliveryamount) ? $data_temp->deliveryamount : 0;
        // data buat dicreate ke db
        $data = [];
        $data['notes'] = $data_temp->notes ? $data_temp->notes : '';
        $data['no_order'] = $data_temp->ordernofe;
        $customer = Customer::where('fe_id', $data_temp->customerfrontend_id)->first();
        $data['id_customer'] = $customer ? $customer->id : 0;
        $data['order_date'] = $data_temp->orderdate;
        $data['payment_type'] = $data_temp->paymenttype;
        $data['payment'] = $paymentamount;
        $data['discount'] = $discount;
        $data['point_usage'] = 0;
        $data['poin_redem'] = $data_temp->point_use;
        $data['shipment_fee'] = !0;
        $data['total'] = $paymentamount;
        $data['status_order'] = $data_temp->status;
        $data['lastsynctoktok'] = $data_temp->updated_at;
        $data['deliveryname'] = $data_temp->deliveryname;
        $data['deliveryaddress'] = $data_temp->deliveryaddress;
        $data['deliveryrt'] = $data_temp->deliveryrt;
        $data['deliveryrw'] = $data_temp->deliveryrw;
        $data['deliveryprovinsi'] = $data_temp->deliveryprovinsi;
        $data['deliverykota'] = $data_temp->deliverykota;
        $data['deliverykecamatan'] = $data_temp->deliverykecamatan;
        $data['deliverykelurahan'] = $data_temp->deliverykelurahan;
        $data['deliveryphone'] = $data_temp->deliveryphone;
        $data['kode_sp'] = $data_temp->kode_sp;
        $data['needsyncvendor'] = 1;
        return $data;
    }

    public static function getdataordertovendor($data = '', $details = '')
    {
        $order = [];
        $customer = Customer::find($data->id_customer);
        $order['customer']['customerfrontend_id'] = $customer->fe_id;
        $order['customer']['id'] = $customer->vendor_outlet_id;
        $order['delivery_name'] = $data->deliveryname;
        $order['delivery_address'] = $data->deliveryaddress;
        $order['delivery_provinsi'] = $data->deliveryprovinsi;
        $order['delivery_kota'] = $data->deliverykota;
        $order['delivery_kecamatan'] = $data->deliverykecamatan;
        $order['delivery_kelurahan'] = $data->deliverykelurahan;
        $order['delivery_phone'] = $data->deliveryphone;
        $order['notes'] = $data->notes ? $data->notes: '';
        $order['payment_type'] = $data->payment_type;
        $order['point_usage'] = $data->point_usage; 
        $order['discountamount'] = 0;
        $order['status'] = $data->status_order;
        $order['customer']['owner_outlet_firstname'] = $customer->firstname;
        $order['customer']['owner_outlet_lastname'] = $customer->lastname;
        $order['customer']['npwp_type'] = 'NON-PKP';
        $order['shippingaddress'] = $data->deliveryaddress . ', ' . $data->deliveryprovinsi;
        $order['kode_sp'] = $data->kode_sp;
        $order['ordernofe'] = $data->no_order;
        $order['orderdate'] = $data->order_date;
        $order['customergroup_code'] = MerchantSetup::getActiveMerchant()->customergroup_code;
        $order['merchant_id'] = MerchantSetup::getActiveMerchant()->merchant_id;
        $order['paymentamount'] = $data->payment;
        $order['total'] = $data->total;
        $order['cus_merchant_id'] = $customer->fe_id;

        foreach($details as $key => $detail)
        {
            $order['detail'][$key]['qty'] = $detail->qty;
            $order['detail'][$key]['price'] = $detail->price;
            $order['detail'][$key]['sku'] = $detail->sku;
            $order['detail'][$key]['nama'] = $detail->nama_fe;
            $order['detail'][$key]['uom'] = $detail->uom;
            $order['detail'][$key]['conversion'] = $detail->conversion;
            $order['detail'][$key]['discamount'] = 0;
        }
        return $order;
    }

    public static function olahDataProduct($data)
    {
        $product = [];
        $product['sku'] = $data->sku;
        $product['product_name'] = $data->nama;
        $product['vendor_id'] = 0;
        $product['short_description'] = $data->short_description;
        $product['desc'] = $data->description;
        $product['merk'] = $data->merek;
        $product['unit'] = $data->unit;
        $product['multiple_unit'] = $data->isiUnit;
        $product['conversion'] = $data->mwconversion;
        $product['package_long'] = $data->packaging_panjang_cm;
        $product['package_height'] = $data->packaging_tinggi_cm;
        $product['dimension_long'] = $data->dimensi_panjang_cm;
        $product['dimension_width'] = $data->dimensi_lebar_cm;
        $product['dimension_height'] = $data->dimensi_tinggi_cm;
        $product['nett'] = $data->berat_bersih_kg;
        $product['sertifikasi'] = $data->sertifikasi;
        $product['lastsyncvendor'] = $data->updated_at;
        $product['needsynctoktok'] = 1;
        return $product;
    }

    public static function olahDataProductInactive($data)
    {
        $product = [];
        $product['sku'] = $data->sku;
        $product['needsynctoktok'] = 1;
        $product['lastsyncvendor'] = $data->updated_at;
        $product['status_approve'] = $data->status_approve;
        $product['inactive'] = $data->inactive;
        return $product;
    }

    public static function olahDataPrice($data)
    {
        $price = [];
        $price['zona_id'] = 0;
        $price['vendor_price'] = $data->price_package_w;
        $price['kode_sp'] = $data->kode_sp;
        $price['sku'] = $data->sku;
        $price['valid_on'] = $data->valid_on; 
        $price['needsynctoktok'] = 1;
        $price['lastsyncvendor'] = $data->updated_at;
        return $price;
    }

    public static function olahDataStock($data)
    {
        $stock = [];
        $stock['zona_id'] = 0;
        $stock['qty'] = $data->available_qty_pcs;
        $stock['min_stock'] = $data->min_qty_pcs;
        $stock['kode_sp'] = $data->kode_sp;
        $stock['sku'] = $data->sku;
        $stock['needsynctoktok'] = 1;
        $stock['lastsyncvendor'] = $data->updated_at;
        return $stock;
    }

    public static function updateProduct($data = '')
    {
        $product = Product::where('sku', $data['sku'])->lockForUpdate()->first();
        $product->update($data);
        return ['result' => 1];
    }

    public static function updateProductInactive($data = '')
    {
        $product = ProductInactive::where('sku', $data->sku)->lockForUpdate()->first();
        $product->update($data);
        return ['result' => 1];
    }

    public static function updatePrice($data = '')
    {
        $price = Price::where('sku', $data['sku'])->where('kode_sp', $data['kode_sp'])->first();
        $price->update($data);
        return ['result' => 1];
    }

    public static function updateStock($data = '')
    {
        $stock = Stock::where('sku', $data['sku'])->where('kode_sp', $data['kode_sp'])->first();
        $stock->update($data);
        return ['result' => 1];
    }

    public static function storeProduct($data = '')
    {
        Product::create($data);
        return ['result' => 1];
    }

    public static function storeProductInactive($data = '')
    {
        ProductInactive::create($data);
        return ['result' => 1];
    }

    public static function storePrice($data = '')
    {
        $price = Price::create($data);
        return ['result' => 1];
    }

    public static function storeStock($data = '')
    {
        $stock = Stock::create($data);
        return ['result' => 1];
    }

    public static function storeMwStatus($mw_status)
    {
        $updated = 0;
        foreach($mw_status->data as $data)
        {
            $no_order = str_replace(MerchantSetup::getActiveMerchant()->prefix, '', $data->ordernofe);
            $order = Order::where('no_order', $no_order)->lockForUpdate()->first();
            if($order)
            {
                $updated++;
                if(trim($data->mw_status) == 'Received MW')
                {
                    $order->needsynctoktok = 1;
                    $order->mw_status = 'Received by IDM';
                    $order->lastsyncmwstatusvendordate = $data->lastmwsynchdate;
                    $order->notes = $data->notes;
                    $order->lastmwsyncdate = $data->lastmwsynchdate;
                    $order->update();
                    self::storeOrderStatusReceivedMw($data);
                }
                elseif(trim($data->mw_status) == '1-Data Receive')
                {
                    $order->needsynctoktok = 0;
                    $order->mw_status = 'Processing';
                    $order->lastsyncmwstatusvendordate = $data->lastmwsynchdate;
                    $order->notes = $data->notes;
                    $order->lastmwsyncdate = $data->lastmwsynchdate;
                    $order->update();
                }
                elseif(trim($data->mw_status) == '3-Data Shipment')
                {
                    $order->needsynctoktok = 0;
                    $order->mw_status = 'Shipment';
                    $order->lastsyncmwstatusvendordate = $data->lastmwsynchdate;
                    $order->notes = $data->notes;
                    $order->lastmwsyncdate = $data->lastmwsynchdate;
                    $order->update();
                }
                elseif(trim($data->mw_status) == '4-Shipment Complete')
                {
                    $order->needsynctoktok = 0;
                    $order->mw_status = 'Customer Received';
                    $order->lastsyncmwstatusvendordate = $data->lastmwsynchdate;
                    $order->notes = $data->notes;
                    $order->lastmwsyncdate = $data->lastmwsynchdate;
                    $order->update();
                }
                // else
                // {
                //     $order->mw_status = 'Failed';
                //     $order->lastsyncmwstatusvendordate = $data->lastmwsynchdate;
                //     $order->update();
                // }
            }
        }
        return $updated;
    }

    public static function storeOrderStatusReceivedMw($order)
    {
        $no_order = str_replace(MerchantSetup::getActiveMerchant()->prefix, '', $order->ordernofe);
        $check_status = OrderStatus::where('ordernofe', $no_order)->where('orderstat', 'Received by IDM')->count();
        if($check_status == 0)
        {
            $data = [];
            $data['ordernofe'] = $no_order;
            $data['kode_sp'] = $order->kode_sp;
            $data['orderstat'] = 'Received by IDM';
            $data['mwtimestamp'] = $order->lastmwsynchdate;
            $data['receiver'] = '-';
            $data['mw_statusdate'] = $order->lastmwsynchdate;
            $data['syncdate'] = $order->updated_at;
            $data['lastmwsyncdate'] = $order->lastmwsynchdate;
            $data['doctype'] = 'SLS';
            $data['detail'] = '-';
            $data['created_by'] = 'cronjob';
            $data['updated_by'] = 'cronjob';
            $data['substat'] = '-';
            OrderStatus::create($data);
        }
    }

    public static function olahDataOrderStatus($status)
    {
        $data = [];
        $no_order = str_replace(MerchantSetup::getActiveMerchant()->prefix, '', $status->ordernofe);
        $data['ordernofe'] = $no_order;
        $data['kode_sp'] = $status->kode_sp;
        $data['orderstat'] = $status->orderstat;
        $data['mwtimestamp'] = $status->mw_timestamp;
        $data['receiver'] = $status->receiver ? $status->receiver : '';
        $data['mw_statusdate'] = $status->mw_statusdate ? $status->mw_statusdate : '0000-00-00 00:00:00';
        $data['syncdate'] = $status->synchdate ? $status->synchdate : '0000-00-00 00:00:00';
        $data['lastmwsyncdate'] = $status->lastmwynchdate ? $status->lastmwynchdate : '0000-00-00 00:00:00';
        $data['doctype'] = $status->doctype ? $status->doctype : '';
        $data['detail'] = $status->detail ? $status->detail : '';
        $data['created_by'] = 'cronjob';
        $data['updated_by'] = 'cronjob';
        $data['lastsyncvendor'] = $status->updated_at;
        $data['substat'] = $status->substat;
        $data['mars_do_no'] = $status->mars_do_no ?? '';
        $data['mars_inv_no'] = $status->mars_inv_no ?? '';
        return $data;
    }

    public static function olahDataOrderTracking($tracking)
    {
        $no_order = str_replace(MerchantSetup::getActiveMerchant()->prefix, '', $tracking->ordernofe);
        $data = [];
        $data['ordernofe'] = $no_order;
        $data['date'] = $tracking->date;
        $data['activities'] = $tracking->activities;
        $data['notes'] = $tracking->notes;
        $data['lastsyncvendor'] = $tracking->updated_at;
        return $data;
    }

    public static function olahDatarevison($value)
    {

        $data = [];
        $data['ordernofe'] =  $value->order->ordernofe;
        $data['status']  = $value->status;
        $data['desc'] = $value->desc;
        $data['data'] = $value->data;
        $data['message'] = $value->message;
        $data['rev_number'] = $value->rev_number;
        $data['created_by'] = 'cronjob';
        $data['updated_by'] = 'cronjob';
        $data['lastsynctoktok'] = $value->updated_at;
        $result = Paidsorevreq::create($data);
        foreach ($value->product_revision as $val) {
          self::olahDataresult($val, $result->id);
        }
        if($value->approval != null)
        {
            self::olahDatarevisonApproval($value->approval,$result->id);
        }

        return $data;
    }
    public static function olahDataresult($value, $id)
    {
        $data = [];
        $data['revreq_id'] = $id;
        $data['sku'] = $value->sku;
        $data['status'] = $value->status;
        $data['notes'] = $value->notes;
        $data['message'] = $value->message;
        $data['desc'] = $value->desc;
        $data['qty'] = $value->qty_before;
        $data['qty_after'] = $value->qty_after;
        $data['price'] = $value->vendor_price;
        $data['price_after'] = $value->vendor_price;
        $data['discamount'] = $value->discamount_before;
        $data['discamount_after'] = $value->discamount_after;
        $data['discidm'] = $value->discidm;
        $data['discidm_after'] = $value->discidm_after;
        $data['created_by'] = 'cronjob';
        $data['updated_by'] = 'cronjob';
        $data['lastsynctoktok'] = $value->updated_at;
        Paidsorevresult::create($data);
        return $data;

    }

    public static function olahDatarevisonApproval($val, $id)
    {
        $data = [];
        $data['revreq_id'] = $id;
        $data['status']     = 1;
        $data['followup']   = $val->follow_up;
        $data['message']    = $val->nessage;
        $data['notes']      = $val->notes;
        $data['created_by'] = 'cronjob';
        $data['updated_by'] = 'cronjob';
        Paidsorevapproval::create($data);
        return ['result' => 1];
    }

    public static function updatepaidReq($data)
    {
        $paid = Paidsorevreq::find($data->id);
        $paid->status= 1;
        $paid->update();
        return ['result' => 1];
    }
    public static function createDataresuffle($value, $param, $orderresufleid="0")
    {

        $data = [];
        $data['frontend_id']        = $value->frontend_id;
        $data['order_id']           = $value->order_id;
        $data['outlet_id']          = $value->outlet_id;
        $data['status_return']      = $value->status_return;
        $data['status_request']     = $value->status_request;
        $data['status_mw']          = $value->frontend_id;
        $data['status_mw_detail']   = $value->frontend_id;
        $data['mw_sync_date'] = $value->mw_sync_date;
        $data['fe_sync_date'] = $value->fe_sync_date;
        $data['request_date'] = $value->request_date;
        $data['return_date_complete'] = $value->return_date_complete;
        $data['note']           = $value->note;
        $data['desc']           = $value->desc;
        $data['remark']         = $value->remark;
        $data['created_by']     = $value->created_by;
        $data['updated_by']     = $value->updated_by;
        $data['retur_number']   = $value->retur_number;
        $data['order_number']   = $value->order_number;
        $data['status_nkr']     = $value->status_nkr;
        $data['nkr_value']      = $value->nkr_value;
        $data['photo_url']      = $value->photo_url;
        $data['jadwal_resuffle']= $value->jadwal_resuffle;
        $data['kode_sp']        = $value->kode_sp;
        $data['revision']       = $value->revision;
        $data['status_fe']      = $value->status_fe;
        $data['status_mw_nkr']  = $value->status_mw_nkr_detail;
        $data['revision_nkr']   = $value->revision_nkr;
        $data['lastsynctoktok'] = $value->updated_at;
        if($param == 'update')
        {
            $result = OrderResuffle::where('order_number', $value->order_number)->update($data);
            foreach ($value->detail as $key => $value) {
               self::olahOrderResuffleDetail($value, $orderresufleid, $param);
            }
        }else{

            $result = OrderResuffle::create($data);
            foreach ($value->detail as $key => $value) {
               self::olahOrderResuffleDetail($value, $result->id, $param);
            }
            
        }  
       
        return $result;

    }

    public static function olahOrderResuffleDetail($value, $id, $param)
    {

        $data                       = [];
        $data['return_id']          = $id;
        $data['sku']                = $value->sku;
        $data['qty']                = $value->qty;
        $data['uom']                = $value->uom;
        $data['reason']             = $value->reason;
        $data['qty_return']         = $value->qty_return;
        $data['qty_order']          = $value->qty_order;
        $data['orderdetail_id']     = $value->orderdetail_id;
        $data['unit_price_order']   = $value->unit_price_order;
        $data['discount']           = $value->discount;
        $data['amount_order']       = $value->amount_order;
        $data['unit_price_nkr']     = $value->unit_price_nkr;
        $data['amount_nkr']         = $value->amount_nkr;
        $data['photo_url']          = $value->photo_url;
        $data['amount_nkr']         = $value->amount_nkr;
        $data['qty_nkr']            = $value->qty_nkr;
        $data['ordernofe']          = $value->ordernofe;
        $data['notes_nkr']          = $value->notes_nkr;
        $data['rma_id_fe']          = $value->rma_id_fe;
        $data['lastsynctoktok']     = $value->updated_at;
        if($param == 'update')
        {
            OrderResuffleDetail::where('ordernofe', $value->ordernofe)->where('sku', $value->sku)->update($data);
        }else{
            
            OrderResuffleDetail::create($data);
        }
        
    }
    public static function olahDataPostresuffle($value)
    {
        $data = [
        'id'                 => $value->id,
        'ordernofe'          =>$value->order_number,
        'frontend_id'        => $value->frontend_id,
        'order_id'           => $value->order_id,
        'outlet_id'          => $value->outlet_id,
        'status_return'      => $value->status_return,
        'status_request'     => $value->status_request,
        'status_mw'          => $value->frontend_id,
        'status_mw_detail'   => $value->frontend_id,
        'mw_sync_date' => $value->mw_sync_date,
        'fe_sync_date' => $value->fe_sync_date,
        'request_date' => $value->request_date,
        'return_date_complete' => $value->return_date_complete,
        'note'           => $value->note,
        'desc'           => $value->desc,
        'remark'         => $value->remark,
        'created_by'     => $value->created_by,
        'updated_by'     => $value->updated_by,
        'retur_number'   => $value->retur_number,
        'order_number'   => $value->order_number,
        'status_nkr'     => $value->status_nkr,
        'nkr_value'      => $value->nkr_value,
        'photo_url'      => $value->photo_url,
        'jadwal_resuffle'=> $value->jadwal_resuffle,
        'kode_sp'        => $value->kode_sp,
        'revision'       => $value->revision,
        'status_fe'      => $value->status_fe,
        'status_mw_nkr'  => $value->status_mw_nkr_detail,
        'revision_nkr'   => $value->revision_nkr,
        'ordernofe'      => $value->order_number,
        'customergroup_code' => MerchantSetup::getActiveMerchant()->customergroup_code,
        'merchant_id'    => MerchantSetup::getActiveMerchant()->merchant_id,
        'detail'         => $value->detail,
        'tracking'       => $value->tracking,
        'verification'   => $value->verification,
        ];
      return $data;
        
    }
}
