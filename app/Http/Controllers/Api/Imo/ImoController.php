<?php

namespace App\Http\Controllers\Api\Imo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Controllers\Api\Imo\ImoHelper;
use App\Http\Controllers\Api\Imo\ImoService;
use App\Http\Controllers\Vendor\VendorService;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use DB;
use Flash;

use App\Models\Customer;
use App\Models\MerchantSetup;
use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\OrderTracking;
use App\Models\Price;
use App\Models\Product;
use App\Models\ProductInactive;
use App\Models\Stock;
use App\Models\Paidsorevreq;
use App\Models\Paidsorevresult;
use App\Models\Paidsorevapproval;
use App\Models\OrderResuffle;
use App\Models\OrderResuffleDetail;

class ImoController extends Controller
{
    public function __construct()
    {
        $this->limit = 500;
        $this->vendor_service = new VendorService;
        $this->toktok_service = new ImoService;
    }

    public function getdatavendor($value='')
    {
       $getdatavendorimo = $this->toktok_service->getdatavendor();
        $storedata = ImoHelper::setdatavendor($getdatavendorimo);
      //   return $getdatavendorimo;


            // return $value;
            // $storedata = ImoHelper::storeproduct($value);
            // $setdata   = ImoHelper::setdataproduct($storedata);
            // $senddata  = ImoService::postdataproduct($setdata);
        // \Log::error(json_encode([$storedata]));
        return $storedata;
    }    

    // public static function getdatavendorbtn($value='')
    // {
    //     $data = $this->getdatavendor();

    //     return back()->with('status', 'Vendor updated!');
    // }

    public function getdataorder()
    {
        $lastorder = Order::orderBy('lastsynctoktok', 'desc')->first();
        $updated_at = $lastorder ? $lastorder->lastsynctoktok : '0000-00-00 00:00:00';
        $order = json_decode($this->toktok_service->getdataordertoktok($updated_at));
        $created = 0;
        $updated = 0;
        $not_updated = 0;
        $total = count($order->data);
        if($order->result == 1)
        {
            foreach($order->data as $data_temp)
            {
                $count = Order::where('no_order', $data_temp->ordernofe)->count();
                // jika datanya ada, bandingkan lastsynctoktok
                if($count > 0)
                {
                    // update order
                    // $order = Order::where('no_order', $data_temp->ordernofe)->first();
                    // if(Carbon::parse($data_temp->updated_at) > Carbon::parse($order->lastsynctoktok))
                    // {
                    //     $data = ImoHelper::olahDataOrder($data_temp, $data_temp->details, $order->id);
                    //     $response = ImoHelper::updateOrder($data, $data_temp->details, $order->id);
                    //     if($response['result'] == 1)
                    //     {
                    //         $updated++;
                    //     }
                    // }
                    // else
                    // {
                    //     $not_updated++;
                    // }
                }
                // jika tidak ada, create ke db
                else
                {
                    // cek customer dulu
                    $check_cust = Customer::where('fe_id', $data_temp->customerfrontend_id)->count();
                    if($check_cust > 0)
                    {
                        $data = ImoHelper::olahDataOrder($data_temp);
                        $response = ImoHelper::storeOrder($data, $data_temp->details);
                        Log::info(json_encode($response));
                        if($response['result'] == 1)
                        {
                            $created++;
                        }
                    }
                    else
                    {
                        Log::info('order belum masuk karena customer belum masuk untuk no order ' . $data_temp->ordernofe);
                        return response(['total' => $total, 'created' => $created, 'updated' => $updated, 'not_updated' => $not_updated]);
                    }
                }
            }
        }
        return response(['total' => $total, 'created' => $created, 'updated' => $updated, 'not_updated' => $not_updated]);
    }

    public function getmwstatus()
    {
        $lastsyncvendor = Order::orderBy('lastsyncmwstatusvendordate', 'desc')->first();
        $lastsyncvendor = $lastsyncvendor ? $lastsyncvendor->lastsyncmwstatusvendordate : '0000-00-00 00:00:00';
        $mw_status = $this->vendor_service->getdatamwstatus($lastsyncvendor);
        $total = count($mw_status->data);
        $updated = 0;
        Log::info(json_encode(['data order utk mw status dari idmarco' => $mw_status]));
        $response = ImoHelper::storeMwStatus($mw_status);

        return response(['total' => $total, 'updated' => $response]);
    }

    public function postmwstatus()
    {
        $status = \DB::table('orders')->where('needsynctoktok', 1)->take($this->limit)->get();
        $total = count($status);
        $success = 0;

        foreach($status as $st)
        {
            $result = $this->toktok_service->postmwstatus($st);
            Log::info(json_encode(['response post mw status ke imo toktok' => $result]));
            if($result->result == 1)
            {
                $updatestatus = Order::lockForUpdate()->find($st->id);
                $updatestatus->needsynctoktok = 0;
                $updatestatus->lastsyncmwstatustoktokdate = $result->data->updated_at;
                $updatestatus->update();
                $success++;
            }
        }
        return response(['total' => $total, 'success' => $success]);
    }

    public function getdatacustomer($value='')
    {
        $updated_at = Customer::orderBy('lastsynctoktok', 'desc')->first();
        $updated_at = !empty($updated_at) ? $updated_at->lastsynctoktok : '0000-00-00 00:00:00';
        $getdatacustomer = $this->toktok_service->getdatacustomer($updated_at);
        Log::info('data customer dari toktok' . json_encode($getdatacustomer));
        $storedata = ImoHelper::setdatacustomerbaru(json_decode($getdatacustomer));
        // Log::info(json_encode(['get data customer status' => $storedata]));
        // return back()->with('status', 'Customer updated!');
        return $storedata;
    }

    public function getdatacustomerbtn($value='')
    {
        $data = $this->getdatacustomer();        
        return back()->with('status', 'Customer updated!');
    }

    public function postdatacustomer()
    {
        $customers = Customer::whereNull('vendor_outlet_id')
                        ->take($this->limit)
                        ->get();
        $total = count($customers);
        $success = 0;

        foreach($customers as $customer)
        {
            $data = [];
            $data['first_name'] = $customer->firstname;
            $data['lastname'] = $customer->lastname;
            $data['no_hp'] = $customer->notelp_cust;
            $data['alamat'] = $customer->alamat;
            $data['province'] = $customer->provinsi;
            $data['city'] = $customer->kota;
            $data['district'] = $customer->kecamatan;
            $data['subdistrict'] = $customer->kelurahan;
            $data['merchant_id'] = MerchantSetup::getActiveMerchant()->merchant_id;
            $result = $this->vendor_service->postcustomer($data);
            Log::info(json_encode($result));
            if($result->result == 1)
            {
                $customer->vendor_outlet_id = $result->data->id;
                $customer->lastsyncvendor = $result->data->updated_at;
                $customer->update();
                $success++;
            }
        }
        return response(['total' => $total, 'success' => $success]);
    }

    public function postdataproduct()
    {
        $start = microtime(true);
        $products = Product::orderBy('lastsyncvendor', 'asc')
                        ->whereNull('lastsynctoktok')
                        ->orWhere('needsynctoktok', 1)
                        ->take($this->limit)
                        ->get();

        $total = count($products);
        $success = 0;

        foreach($products as $product)
        {
            Log::info(json_encode(['data push product ke toktok sku' => $product]));
            $product_arr = $product->toArray();
            $product_arr['product_name'] = base64_encode($product_arr['product_name']);
            $product_arr['desc'] = base64_encode(htmlentities($product_arr['desc']));
            $result = $this->toktok_service->postproduct($product_arr);
            Log::info(json_encode(['response push product dari toktok sku=' . $product->sku => $result]));
            if($result->result == 1)
            {
                $product->lastsynctoktok = $result->data->updated_at;
                $product->needsynctoktok = 0;
                $product->update();
                $success++;
            }
        }
        $end = microtime(true);
        $time = number_format(($end - $start), 2);
        Log::info(json_encode('post data product to TokTok time execution : '.$time));
        return response(['total' => $total, 'success' => $success]);
    }

    public function postProductInactive()
    {
        $start = microtime(true);
        $products = ProductInactive::orderBy('lastsyncvendor', 'asc')
                        ->where('needsynctoktok', 1)
                        ->whereHas('product', function($q) {
                            $q->where('needsynctoktok', 0);
                        })
                        ->take($this->limit)
                        ->get();

        $total = count($products);
        $success = 0;

        foreach($products as $product)
        {
            Log::info(json_encode(['data push product inactive ke toktok sku' => $product]));
            $result = $this->toktok_service->postproductinactive($product);
            Log::info(json_encode(['response push product dari toktok sku=' . $product->sku => $result]));
            if($result->result == 1)
            {
                if($result->success == 1)
                {
                    $product->lastsynctoktok = $result->data->updated_at;
                    $product->needsynctoktok = 0;
                    $product->update();
                    $success++;
                }
                else
                {
                    // kalau ga ada skunya
                    $product->needsynctoktok = 0;
                    $product->update();
                }
            }
        }
        $end = microtime(true);
        $time = number_format(($end - $start), 2);
        Log::info(json_encode('post data product inactive to TokTok time execution : '.$time));
        return response(['total' => $total, 'success' => $success]);
    }

    public function postdataprice()
    {
        $start = microtime(true);
        $prices = Price::orderBy('lastsyncvendor', 'asc')
                        ->whereNull('lastsynctoktok')
                        ->orWhere('needsynctoktok', 1)
                        ->take($this->limit)
                        ->get();

        $total = count($prices);
        $success = 0;

        foreach($prices as $price)
        {
            $result = $this->toktok_service->postprice($price);
            Log::info(json_encode(['response push price ke toktok sku=' . $price->sku . ' kode_sp=' . $price->kode_sp => $result]));
            if($result->result == 1)
            {
                if($result->success == 1)
                {
                    $price->lastsynctoktok = $result->data->updated_at;
                    $price->needsynctoktok = 0;
                    $price->update();
                    $success++;
                }
                else
                {
                    $price->delete();
                    $success++;
                }
            }
        }
        $end = microtime(true);
        $time = number_format(($end - $start), 2);
        Log::info(json_encode('post data price to TokTok time execution : '.$time));
        return response(['total' => $total, 'success' => $success]);
    }

    public function postdatastock()
    {
        $start = microtime(true);
        $stocks = Stock::orderBy('lastsyncvendor', 'asc')
                        ->whereNull('lastsynctoktok')
                        ->orWhere('needsynctoktok', 1)
                        ->take($this->limit)
                        ->get();

        $total = count($stocks);
        $success = 0;

        foreach($stocks as $stock)
        {
            $result = $this->toktok_service->poststock($stock);
            Log::info(json_encode(['response push stock ke toktok sku=' . $stock->sku . ' kode_sp=' . $stock->kode_sp => $result]));
            if($result->result == 1)
            {
                if($result->success == 1)
                {
                    $stock->lastsynctoktok = $result->data[0]->updated_at;
                    $stock->needsynctoktok = 0;
                    $stock->update();
                    $success++;
                }
                else
                {
                    $stock->delete();
                    $success++;
                }
            }
        }
        $end = microtime(true);
        $time = number_format(($end - $start), 2);
        Log::info(json_encode('post data stock to TokTok time execution : '.$time));
        return response(['total' => $total, 'success' => $success]);
    }    

    public function postdataorder()
    {
        $orders = Order::where('needsyncvendor', 1)->take($this->limit)->get();
        $total = count($orders);
        $success = 0;

        foreach($orders as $order)
        {
            $customer = Customer::find($order->id_customer);
            if(!empty($customer) && !empty($customer->vendor_outlet_id))
            {
                $data = ImoHelper::getdataordertovendor($order, $order->detail);
                $result = $this->vendor_service->postorder($data);
                Log::info(json_encode(['response post order dari imo idmarco ' . $order->no_order => $result]));
                if($result->result == 1)
                {   
                    if($result->error == 'true')
                    {
                        $order->needsynctoktok = 1;
                        $order->notes = $result->message;
                        $order->mw_status = 'Failed';
                    }
                    $order->needsyncvendor = 0;
                    $order->lastsyncvendor = Carbon::now()->toDateTimeString();
                    $order->update();
                    $success++;
                }
            }
        }
        return response(['total' => $total, 'success' => $success]);
    }

    public function getdataproduct()
    {
        $start = microtime(true);
        $check = Product::orderBy('lastsyncvendor', 'desc')->first();
        $updated_at = !empty($check) ? $check->lastsyncvendor : '0000-00-00 00:00:00';
        $products = $this->vendor_service->getproduct($updated_at);
        Log::info(json_encode(['data product dari imo idmarco' => $products]));
        $created = 0;
        $updated = 0;
        $not_updated = 0;
        $total = count($products->data);
        if($products->result == 1)
        {
            foreach($products->data as $product)
            {
                $data = ImoHelper::olahDataProduct($product);
                $check_product = Product::where('sku', $product->sku)->count();
                if($check_product > 0)
                {
                    $product_check = Product::where('sku', $product->sku)->first();
                    if(Carbon::parse($product->updated_at) > Carbon::parse($product_check->lastsyncvendor))
                    {
                        $response = ImoHelper::updateProduct($data);
                        if($response['result'] == 1)
                        {
                            $updated++;
                        }
                    }
                    else
                    {
                        $not_updated++;
                    }
                }
                else
                {
                    $response = ImoHelper::storeProduct($data);
                    if($response['result'] == 1)
                    {
                        $created++;
                    }
                }
            }
        }
        $end = microtime(true);
        $time = number_format(($end - $start), 2);
        Log::info(json_encode('get data product from IDM time execution : '.$time));
        return response(['total' => $total, 'created' => $created, 'updated' => $updated, 'not_updated' => $not_updated]);
    }    


    public function getdataproductbtn()
    {
         $check = Product::orderBy('lastsyncvendor', 'desc')->first();
        $updated_at = !empty($check) ? $check->lastsyncvendor : '0000-00-00 00:00:00';
        $products = $this->vendor_service->getproduct($updated_at);
        Log::info(json_encode(['data product dari imo idmarco' => $products]));
        $created = 0;
        $updated = 0;
        $not_updated = 0;
        $total = count($products->data);
        if($products->result == 1)
        {
            foreach($products->data as $product)
            {
                $data = ImoHelper::olahDataProduct($product);
                $check_product = Product::where('sku', $product->sku)->count();
                if($check_product > 0)
                {
                    $product_check = Product::where('sku', $product->sku)->first();
                    if(Carbon::parse($product->updated_at) > Carbon::parse($product_check->lastsyncvendor))
                    {
                        $response = ImoHelper::updateProduct($data);
                        if($response['result'] == 1)
                        {
                            $updated++;
                        }
                    }
                    else
                    {
                        $not_updated++;
                    }
                }
                else
                {
                    $response = ImoHelper::storeProduct($data);
                    if($response['result'] == 1)
                    {
                        $created++;
                    }
                }
            }
        }

            return back()->with('status', 'Product updated!');
    }

    public function getorderstatus()
    {
        $check = OrderStatus::orderBy('lastsyncvendor', 'desc')->first();
        $updated_at = !empty($check) ? $check->lastsyncvendor : '0000-00-00 00:00:00';
        $order_status = $this->vendor_service->getorderstatus($updated_at);
        Log::info(json_encode(['data paidsalesorderdelv dari imo idmarco' => $order_status]));
        $created = 0;
        $total = count($order_status->data);
        if($order_status->result == 1)
        {
            foreach($order_status->data as $status)
            {
                // jika order status bukan dari jatake dan status 4
                if($status->kode_sp != '520052100024' || $status->orderstat != '4-Shipment Complete')
                {
                    $data = ImoHelper::olahDataOrderStatus($status);
                    $check_status = OrderStatus::where('ordernofe', $data['ordernofe'])->where('orderstat', $data['orderstat'])->where('kode_sp', $data['kode_sp'])->where('detail', $data['detail'])->where('mwtimestamp', $data['mwtimestamp'])->count();
                    if($check_status == 0)
                    {
                        OrderStatus::create($data);
                        $created++;
                    }
                }
            }
        }
        return response(['total' => $total, 'created' => $created]);
    }

    public function getordertracking()
    {
        $check = OrderTracking::orderBy('lastsyncvendor', 'desc')->first();
        $updated_at = !empty($check) ? $check->lastsyncvendor : '0000-00-00 00:00:00';
        $order_tracking = $this->vendor_service->getordertracking($updated_at);
        Log::info(json_encode(['data paidsalestracing dari imo idmarco' => $order_tracking]));
        $created = 0;
        $total = count($order_tracking->data);
        if($order_tracking->result == 1)
        {
            foreach($order_tracking->data as $tracking)
            {
                $data = ImoHelper::olahDataOrderTracking($tracking);
                $check_tracking = OrderTracking::where('ordernofe', $tracking->ordernofe)->where('notes', $tracking->notes)->count();
                if($check_tracking == 0)
                {
                    OrderTracking::create($data);
                    $created++;
                }
            }
        }
        return response(['total' => $total, 'created' => $created]);
    }

    public function postordertracking()
    {
        $tracking = OrderTracking::whereNull('lastsynctoktok')
                        ->orWhereRaw('lastsyncvendor >= lastsynctoktok')
                        ->take($this->limit)
                        ->get();
        $total = count($tracking);
        $success = 0;

        foreach($tracking as $tr)
        {
            $result = $this->toktok_service->postordertracking($tr);
            Log::info(json_encode(['response post order tracking ke imo toktok' => $result]));
            if($result->result == 1)
            {
                $tr->lastsynctoktok = $result->data->updated_at;
                $tr->update();
                $success++;
            }
        }
        return response(['total' => $total, 'success' => $success]);
    }

    public function postorderstatus()
    {
        $status = OrderStatus::whereNull('lastsynctoktok')
                        ->orderBy('mwtimestamp', 'asc')
                        ->take($this->limit)
                        ->get();
        $total = count($status);
        $success = 0;

        foreach($status as $st)
        {
            $result = $this->toktok_service->postorderstatus($st);
            Log::info(json_encode(['response post order status ke imo toktok' => $result]));
            if($result->result == 1)
            {
                $st->lastsynctoktok = $result->data->updated_at;
                $st->update();
                $success++;
            }
        }
        return response(['total' => $total, 'success' => $success]);
    }

    public function getdataprice()
    {
        $start = microtime(true);
        $check = Price::orderBy('lastsyncvendor', 'desc')->first();
        $updated_at = !empty($check) ? $check->lastsyncvendor : '0000-00-00 00:00:00';
        $prices = $this->vendor_service->getprice($updated_at);
        Log::info(json_encode(['data price dari imo idmarco' => $prices]));
        $created = 0;
        $updated = 0;
        $not_updated = 0;
        $total = count($prices->data);
        if($prices->result == 1)
        {
            foreach($prices->data as $price)
            {
                $data = ImoHelper::olahDataPrice($price);
                $check_price = Price::where('sku', $price->sku)->where('kode_sp', $price->kode_sp)->count();
                if($check_price > 0)
                {
                    $price_check = Price::where('sku', $price->sku)->where('kode_sp', $price->kode_sp)->first();
                    if(Carbon::parse($price->updated_at) > Carbon::parse($price_check->lastsyncvendor))
                    {
                        $response = ImoHelper::updatePrice($data);
                        if($response['result'] == 1)
                        {
                            $updated++;
                        }
                    }
                    else
                    {
                        $not_updated++;
                    }
                }
                else
                {
                    $response = ImoHelper::storePrice($data);
                    if($response['result'] == 1)
                    {
                        $created++;
                    }
                }
            }
        }
        $end = microtime(true);
        $time = number_format(($end - $start), 2);
        Log::info(json_encode('get data price from IDM time execution : '.$time));
        return response(['total' => $total, 'created' => $created, 'updated' => $updated, 'not_updated' => $not_updated]);
    }

    public function getdatastock()
    {
        $start = microtime(true);
        $check = Stock::orderBy('lastsyncvendor', 'desc')->first();
        $updated_at = !empty($check) ? $check->lastsyncvendor : '0000-00-00 00:00:00';
        $stocks = $this->vendor_service->getstock($updated_at);
        Log::info(json_encode(['data stock dari imo idmarco' => $stocks]));
        $created = 0;
        $updated = 0;
        $not_updated = 0;
        $total = count($stocks->data);
        if($stocks->result == 1)
        {
            foreach($stocks->data as $stock)
            {
                if($stock->available_qty_pcs > 0)
                {
                    $data = ImoHelper::olahDataStock($stock);
                    $check_stock = Stock::where('sku', $stock->sku)->where('kode_sp', $stock->kode_sp)->count();
                    if($check_stock > 0)
                    {
                        $stock_check = Stock::where('sku', $stock->sku)->where('kode_sp', $stock->kode_sp)->first();
                        if(Carbon::parse($stock->updated_at) > Carbon::parse($stock_check->lastsyncvendor))
                        {
                            $response = ImoHelper::updateStock($data);
                            if($response['result'] == 1)
                            {
                                $updated++;
                            }
                        }
                        else
                        {
                            $not_updated++;
                        }
                    }
                    else
                    {
                        $response = ImoHelper::storeStock($data);
                        if($response['result'] == 1)
                        {
                            $created++;
                        }
                    }
                }
            }
        }
        $end = microtime(true);
        $time = number_format(($end - $start), 2);
        Log::info(json_encode('get data stock from IDM time execution : '.$time));
        return response(['total' => $total, 'created' => $created, 'updated' => $updated, 'not_updated' => $not_updated]);
    }
    public function Createrevision()
    {

        $check = Paidsorevreq::orderBy('lastsynctoktok', 'desc')->first();
        $updated_at = !empty($check) ? $check->lastsynctoktok : '0000-00-00 00:00:00';
        $result = $this->toktok_service->getdataorderrevisiontoktok($updated_at);
        Log::info(json_encode(['data order revision dari imo toktok' => $result])); 
        $created=0;
        $updated=0;
        $not_updated=0;
        $total= count($result->data);
        foreach($result->data as $value) {
           $checkData = Paidsorevreq::where('ordernofe', $value->order->ordernofe)->where('rev_number', '>=', $value->rev_number)->count();

            if($checkData > 0)
            {
                
                // fungsi update check approval
                $paid_req = Paidsorevreq::where('ordernofe', $value->order->ordernofe)->where('rev_number', '>=', $value->rev_number)->first();
                if($value->approval != null)
                {
                    $checkApprov =Paidsorevapproval::where('revreq_id', $paid_req->id)->count();
                    if($checkApprov > 0)
                    {
                         Log::info('approval tersedia'); 
                       
                    }else{
                        $response = ImoHelper::olahDatarevisonApproval($value->approval, $paid_req->id);
                        $paidreq  = ImoHelper::updatepaidReq($paid_req);
                        if($response['result'] == 1)
                        {
                            $updated++;
                        }
                    }
                    
                }else{
                    $not_updated++;
                }
            }
            else
            {   
                    // fungsi create ke table req dan result and approval if != null
                    $response = ImoHelper::olahDatarevison($value);
                    $created++;

            }


        }
        return response(['total' => $total, 'created' => $created, 'updated'=>$updated, 'not_updated'=>$not_updated]);
        
    }

    public function pushRevision()
    {
        $data   = Paidsorevreq::with('productRevision', 'approval','order')
                        ->whereNull('lastsyncvendor')
                        ->orWhere('lastsynctoktok',  '>=', 'lastsyncvendor')
                        // ->orWhereDoesntHave('approval')
                        ->orderBy('lastsynctoktok', 'desc')->take($this->limit)
                        ->get();
        $total       = count($data);
        $success     = 0;
        $not_updated = 0;
        foreach ($data as $val) {
            Log::info(json_encode(['data post per revision idmarco' => $val])); 
            $result = $this->vendor_service->postrevision($val);
            Log::info(json_encode(['data post revision idmarco' => $result])); 
            if($result->result == 1)
            {
                    if(!empty($result->data))
                    {
                        $val->lastsyncvendor = $result->data->updated_at;
                        $val->update();
                        $success++;
                    }else{
                        $not_updated++;
                    }

            }
        }
        return response(['total' => $total, 'success' => $success, 'not_updated'=> $not_updated]);
    }

    public function get_orderresuffle()
    {

        $check = OrderResuffle::orderBy('lastsynctoktok', 'desc')->first();
        $updated_at = !empty($check) ? $check->lastsynctoktok : '0000-00-00 00:00:00';
        $result = $this->toktok_service->getdataorderresuffletoktok($updated_at);
        Log::info(json_encode(['data order resuffle dari imo toktok' => $result]));

        $total   = count($result->data);
        $created = 0;
        $updated = 0;
        foreach ($result->data as $key => $value) {
            $orderresufle = OrderResuffle::where('order_number', $value->order_number)->first();
            $post   = ImoHelper::olahDataPostresuffle($value);
            if($orderresufle)
            {
                // update
                Log::info(json_encode(['order_number resuffle update'=> $value->order_number]));
                $update = ImoHelper::createDataresuffle($value, 'update', $orderresufle->id);
                Log::info(json_encode(['data update order resuffle ke imo'=>$post]));
                $update_resuffle = $this->vendor_service->update_orderresuffle($post);
                Log::info(json_encode(['response update order resuffle ke imo' => $post_resuffle]));
                if($post_resuffle->result == 1)
                {
                    $orderresufle->lastsyncvendor = $update_resuffle->data->created_at;
                    $orderresufle->update();
                }
                $updated++;
            }else{
                
                // Create
                Log::info(json_encode(['order_number resuffle create'=> $value->order_number]));
                $create = ImoHelper::createDataresuffle($value, 'create');
                Log::info(json_encode(['data post order resuffle ke imo'=>$post]));
                $post_resuffle = $this->vendor_service->post_orderresuffle($post);
                Log::info(json_encode(['response post order resuffle ke imo' => $post_resuffle]));
                if($post_resuffle->result == 1)
                {
                    $update_res = OrderResuffle::find($create->id);
                    $update_res->lastsyncvendor = $post_resuffle->data->created_at;
                    $update_res->update();
                }
                $created++; 
            }           
            
        }
        return response(['total' => $total,'created' => $created, 'updated'=> $updated]);
       
    }

    public function clearlog()
    {
        $free = disk_free_space('/');
        $total = disk_total_space('/');
        if($total - $free > 80/100 * $total)
        {
            unlink(storage_path('logs/laravel.log'));
            return 'success empty log';
        }
    }

    public function getProductInactive()
    {
        $start = microtime(true);
        $check = ProductInactive::orderBy('lastsyncvendor', 'desc')->first();
        $updated_at = !empty($check) ? $check->lastsyncvendor : '0000-00-00 00:00:00';
        $products = $this->vendor_service->getproductinactive($updated_at);
        Log::info(json_encode(['data product inactive dari imo idmarco' => $products]));
        $created = 0;
        $updated = 0;
        $not_updated = 0;
        $total = count($products->data);
        if($products->result == 1)
        {
            foreach($products->data as $product)
            {
                $data = ImoHelper::olahDataProductInactive($product);
                $check_product = ProductInactive::where('sku', $product->sku)->count();
                if($check_product > 0)
                {
                    $product_check = ProductInactive::where('sku', $product->sku)->first();
                    if(Carbon::parse($product->updated_at) > Carbon::parse($product_check->lastsyncvendor))
                    {
                        $response = ImoHelper::updateProductInactive($data);
                        if($response['result'] == 1)
                        {
                            $updated++;
                        }
                    }
                    else
                    {
                        $not_updated++;
                    }
                }
                else
                {
                    $response = ImoHelper::storeProductInactive($data);
                    if($response['result'] == 1)
                    {
                        $created++;
                    }
                }
            }
        }
        $end = microtime(true);
        $time = number_format(($end - $start), 2);
        Log::info(json_encode('get data product inactive from IDM time execution : '.$time));
        return response(['total' => $total, 'created' => $created, 'updated' => $updated, 'not_updated' => $not_updated]);
    }

}
