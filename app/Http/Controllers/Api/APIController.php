<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class APIController extends Controller
{
    public function index(){
    	return view('api.index');
    }

    public function create(){
    	return view('api.form.create');
    }
}
