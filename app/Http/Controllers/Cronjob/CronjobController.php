<?php

namespace App\Http\Controllers\Cronjob;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CronjobController extends Controller
{
    //
    public function __construct()
    {
        # code...
        $this->data['everyexpressionvalues'] =
        [
          "* * * * *" => "minute",
          "*/2 * * * *" => "2 minute",
          "*/3 * * * *" => "3 minute",
          "*/4 * * * *" => "4 minute",
          "*/5 * * * *" => "5 minute",
          "*/6 * * * *" => "6 minute",
          "*/10 * * * *" => "10 minute",
          "*/12 * * * *" => "12 minute",
          "*/15 * * * *" => "15 minute",
          "*/20 * * * *" => "20 minute",
          "*/30 * * * *" => "30 minute",
          "0 * * * *" => "hour",
          "0 */2 * * *" => "2 hour",
          "0 */3 * * *" => "3 hour",
          "0 */4 * * *" => "4 hour",
          "0 */5 * * *" => "5 hour",
          "0 */6 * * *" => "6 hour",
          "0 */8 * * *" => "8 hour",
          "0 */12 * * *" => "12 hour",
          "0 0 * * *" => "day",
          "0 0 */2 * *" => "2 day",
          "0 0 */3 * *" => "3 day",
          "0 0 */5 * *" => "5 day",
          "0 0 */10 * *" => "10 day",
          "0 0 */15 * *" => "15 day",
        ];
    }

    public function routelistcront()
    {
        # code...        
        $route    = collect(\Route::getRoutes())->map(function ($route) {
                    return $route->uri();
                });

        $banyak   = count($route);
        $new_data = [];

        for ($i=0; $i < $banyak; $i++) { 
            # code...
            # cronjob_list/
            if(substr($route[$i], 0, 13) == "cronjob_list/"){
                array_push($new_data, $route[$i]);
            }
        }

        return $new_data;
    }

    public function index($value='')
    {
        # code...       
        return view('cronjob.index')->with($this->data);
    }

    public function create($value='')
    {
        # code...
        $this->data["url"]            = route("cronjob.store");
        $this->data["method"]         = "POST";
        $this->data["list_url_cront"] = $this->routelistcront();
         $this->data["log"]            = [];
        return view('cronjob.form.create')->with($this->data);
    }

    public function edit($id)
    {
        # code...
        $this->data["data"]           = \App\Models\Cronjob::find($id);
        $this->data["url"]            = route("cronjob.update", $id);
        $this->data["method"]         = "PUT";
        $this->data["log"]            = \App\Models\CronjobLog::whereCronjob_id($id)->get();
        $this->data["list_url_cront"] = $this->routelistcront();
        return view('cronjob.form.create')->with($this->data);
    }

    public function store(Request $request)
    {
        # code...
        $input  = $request->all();
        
        $store = \App\Models\Cronjob::create([
            'vendor_id' => $input['vendor_id'],
            'name' => $input['name'],
            // 'description' => $input['description'],
            'time_defined' => 0,
            'request_url' => $input['request_url'],            
            'every_expression' => $input['every_expression'],            
            'status' => $input['status'],
            'status_log' => $input['status_log'],
        ]);

        $setcron = \App\Http\Controllers\Cronjob\CronjobHelper::setcron();

        return redirect()->route('cronjob.index');
    }

    public function update(Request $request, $id)
    {
    	# code...
        $input  = $request->all();
    	
        $store = \App\Models\Cronjob::updateOrCreate(
            [
                'id' => $id,
            ],
            [
            'vendor_id' => $input['vendor_id'],
            'name' => $input['name'],
            // 'description' => $input['description'],
            'time_defined' => 0,
            'request_url' => $input['request_url'],            
            'every_expression' => $input['every_expression'],            
            'status' => $input['status'],
            'status_log' => $input['status_log'],
        ]);

        $setcron = \App\Http\Controllers\Cronjob\CronjobHelper::setcron();

        return redirect()->route('cronjob.index');
    }

    public function setup(){
        return view('cronjob.form.setup');
    }

    public function execution($id)
    {
        # code...
        $logger  = ['cronjob_id' => $id, 'start_time' => date('Y-m-d H:i:s')];
        $findurl = \App\Models\Cronjob::find($id);
        $ch      = curl_init();
        $url     = "$findurl->request_url";

        $ch  = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);        
        $result      = curl_exec($ch);
        $result_info = curl_getinfo($ch);
        // $location = json_decode($server_output, false);
        curl_close($ch);

        $logger['http_code']     = $result_info['http_code'];
        $logger['connect_time']  = round($result_info['connect_time'], 3);
        $logger['total_time']    = round($result_info['total_time'], 3);
        $logger['end_time']      = date('Y-m-d H:i:s');
        $logger['output_length'] = $result_info['size_download'];
        $logger['return_value']  = $result;

        if($findurl->status_log == 1){

            $storehistory = \App\Models\CronjobLog::create([

                'cronjob_id' => $id,
                'message' => $logger['return_value'],
                'status' => $logger['http_code'],
                'last_sync' => date('Y-m-d H:i:s'),
            ]);
        }

        return $logger;

    }
}

