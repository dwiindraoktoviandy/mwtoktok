<?php

namespace App\Http\Controllers\Cronjob;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CronjobHelper extends Controller
{

    public static function setcron($value='')
    {
    	# code...
    	$user_crontab = "www-data";
    	$filename_temp_gen = false;
    	$current_dir = __DIR__;    	
    	$temp_storage_dir = storage_path() . "/framework/cache/";
    	$filename_temp = !$filename_temp_gen ? "00000000000000_cronjob" : date('YmdHis') . "_cronjob";

    	
    		/* load cronjobs */
    		$cronjobs = \App\Models\CronJob::whereStatus(1)->orderBy('id')->get();

    		/* new / open file stream */
    		$f = fopen("$temp_storage_dir/$filename_temp", "w");
    		$begin = "#Shell variable for cron\nSHELL=/bin/bash\n\n#PATH variable for cron\nPATH=/usr/local/bin:/usr/local/sbin:/sbin:/usr/sbin:/bin:/usr/bin:/usr  /bin/X11\n\n#Job List\n";
    		fwrite($f, $begin);

    		/* load expression from data base */    		
    		$result_data = "";
    		$text_expression = "";

    		foreach($cronjobs as $cronjob) {

                $url = route("execution", $cronjob->id);
                $result_data .= "$cronjob->every_expression curl $url\n";
    		}

    		fwrite($f, $result_data);

    		/* close / save file stream */
    		fclose($f);

    		/* after build, add list to crontab */
    		// clear all crontab
    		$cmd1 = exec("crontab -r -u ".$user_crontab);

    		// // set crontab list
    		$cmd2 = exec("crontab $temp_storage_dir/$filename_temp -u ".$user_crontab);
    }
}
