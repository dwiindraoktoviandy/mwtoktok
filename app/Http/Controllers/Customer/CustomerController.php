<?php

namespace App\Http\Controllers\Customer;

use DataTables;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Customer;

class CustomerController extends Controller
{
    public function index(Request $request){
    	if($request->ajax())
    	{
    		$customer = Customer::select([
    			'fe_id',
    			DB::raw("CONCAT(customers.firstname, ' ', customers.lastname) as fullname"),
    			DB::raw("CONCAT(customers.alamat, ', ', customers.kelurahan, ', ', customers.kecamatan, ', ', customers.kota, ', ', customers.provinsi) as address"),
    			'notelp_cust',
    			'vendor_outlet_id'
    		]);
    		return DataTables::eloquent($customer)
    			->filterColumn('fullname', function($query, $keyword) {
    				$sql = "CONCAT(customers.firstname, ' ', customers.lastname) like ?";
    				$query->whereRaw($sql, ["%{$keyword}%"]);
    			})
    			->filterColumn('address', function($query, $keyword) {
    				$sql = "CONCAT(customers.alamat, ', ', customers.kelurahan, ', ', customers.kecamatan, ', ', customers.kota, ', ', customers.provinsi) like ?";
    				$query->whereRaw($sql, ["%{$keyword}%"]);	
    			})
    			->toJson();
    	}
    	return view('customer.index');
    }
}
