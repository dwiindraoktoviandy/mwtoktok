<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;

class UserHelper extends Controller
{
    public function userlist($value='')
    {
    	# code...
    	// return response()->json(['data' => User::selectRaw("id, name, email, username, created_at, updated_at, '<a class=\"btn btn-info btn-sm\" onclick=\"showedit()\">Edit</a>' as action")->orderBy('id', 'desc')->get()], 200);
    	return response()->json(['data' => User::orderBy('id', 'desc')->get()->toArray()], 200);
    }
}
