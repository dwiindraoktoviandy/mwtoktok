<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Redirect;
use App\Models\UserGroup;

class UserGroupsController extends Controller
{
    public function index($value='')
    {
    	# code...
    	$data 	= '';
    	return view('auth.registergroups', ['data' => $data]);
    }

    public function store(Request $request)
    {
    	# code...
    	$input 	                = $request->all();
    	$input['access_route']	= json_encode(['routelist' => $request->input('routelist')]);

    	$store 	                = UserGroup::create($input);

    	return Redirect::back();
    }

    public function edit($value='')
    {
    	# code...
    	$data 	= UserGroup::find($value);
    	return view('auth.registergroups', ['data' => $data]);
    }

    public function update(Request $request, $id)
    {
    	# code...
        $data   = '';
    	$input 	                    = $request->all();
    	$input['access_route'] = json_encode(['routelist' => $request->input('routelist')]);

    	$update 	                = UserGroup::find($id);
    	$update->update($input);

        return view('auth.registergroups', ['data' => $data]);
    }
}
