<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use App\Http\Controllers\Auth\RegisterController;
use Redirect;

use App\User;

class UserController extends RegisterController
{
    use RegistersUsers;

    public function __construct()
	{
		// $this->registercontroller = new RegisterController;	
	}

	public function index($value=''){
		$data = '';
		return view('auth.registers', ['data' => $data]);
	}

	public function store(Request $request){
		$this->validator($request->all())->validate();

		event(new Registered($user = $this->create($request->all())));
		return Redirect::back();
	}

	public function update(Request $request, $id){
		$this->validator($request->all(), User::find($id))->validate();

		event(new Registered($user = $this->create($request->all(), User::find($id))));

		return redirect()->route('user.index');
	}

	public function edit($value=''){
		$data = User::find($value);
		return view('auth.registers', ['data' => $data]);
	}
}
