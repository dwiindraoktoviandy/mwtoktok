<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserGroup;

class UserGroupsHelper extends Controller
{
    public function datalist($value='')
    {
    	# code...    
    	return response()->json(['data' => UserGroup::orderBy('id', 'desc')->get()->toArray()], 200);
    }
}
