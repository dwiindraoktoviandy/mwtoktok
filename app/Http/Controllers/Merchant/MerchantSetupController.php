<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MerchantSetupController extends Controller
{
    public function show()
    {
    	$data = \App\Models\MerchantSetup::count();
    	if($data > 0)
    	{
    		$data = \App\Models\MerchantSetup::first();
    	}
    	else
    	{
    		$data = '';
    	}
    	return view('merchant_setup.show', compact('data'));
    }

    public function store(Request $request)
    {
    	$data = $request->all();
    	$validatedData = $request->validate([
    	    'name' => 'required',
    	    'merchant_id' => 'required',
    	    'customergroup_code' => 'required',
            'prefix' => 'required'
    	]);

    	\App\Models\MerchantSetup::create($data);

    	return back()->with('success', 'Merchant Setup Has Been Saved');
    }

    public function update(Request $request, $id)
    {
    	$data = $request->all();
    	$validatedData = $request->validate([
    	    'name' => 'required',
    	    'merchant_id' => 'required',
    	    'customergroup_code' => 'required',
            'prefix' => 'required'
    	]);

    	$setup = \App\Models\MerchantSetup::find($id);
    	$setup->update($data);

    	return back()->with('success', 'Merchant Setup Has Been Updated');
    }
}
