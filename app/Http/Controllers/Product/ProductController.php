<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Vendor;

class ProductController extends Controller
{
    public function index(){
    	$products = Product::orderBy('id', 'desc')->get();
    	$vendors= Vendor::orderBy('id', 'desc')->get();
    return view('product.index', ['products' => $products ,'vendors' => $vendors]);
    }
}
