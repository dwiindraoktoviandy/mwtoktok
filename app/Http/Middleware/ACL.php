<?php

namespace App\Http\Middleware;

use Closure;

class ACL
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            
            if(\Auth::check()){

                if (in_array(\Route::getCurrentRoute()->getName(), json_decode(\Auth::user()->usergroup->access_route)->routelist))
                    {
                        return $next($request);
                    } else {
                        return redirect()->route('/');
                    }
            }

            // return $next($request);
            return redirect('/login');

        } catch (\Exception $e) {

            return 'acl File '.$e->getMessage().' file '.$e->getFile().' on '.$e->getLine();
        }
    }
}
