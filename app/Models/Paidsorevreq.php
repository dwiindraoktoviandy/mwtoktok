<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Paidsorevreq extends Model
{
	protected $table = 'paidsorevreq';
    protected $guarded = [];

    public function productRevision(){
         return $this->hasMany('App\Models\Paidsorevresult', 'revreq_id', 'id');
    }
    public function approval(){
         return $this->belongsTo('App\Models\Paidsorevapproval', 'id', 'revreq_id');
    }
    public function order(){
         return $this->belongsTo('App\Models\Order', 'ordernofe', 'no_order');
    }
}
