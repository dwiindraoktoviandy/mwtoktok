<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Paidsorevapproval extends Model
{
	protected $table = 'paidsorevapproval';
    protected $guarded = [];
}
