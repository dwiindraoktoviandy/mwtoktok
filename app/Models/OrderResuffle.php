<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderResuffle extends Model
{
	protected $table = 'return';
    protected $guarded = [];

    public function detail() {
        return $this->hasMany('App\Models\OrderResuffleDetail', 'return_id', 'id');
    }
}
