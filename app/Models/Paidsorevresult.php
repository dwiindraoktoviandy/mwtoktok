<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Paidsorevresult extends Model
{
	protected $table = 'paidsorevresult';
    protected $guarded = [];
}
