<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderResuffleDetail extends Model
{
	protected $table = 'return_detail';
    protected $guarded = [];

    public function resuffle() {
        return $this->belongsTo('App\Models\OrderResuffle', 'return_id','id');
    }
}
