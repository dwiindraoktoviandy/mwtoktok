<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    protected $fillable = ['name','description','access_route'];    

    protected $appends = ['action'];

    public function getActionAttribute($value='')
    {
        # code...
        return '<a class="btn btn-complete btn-sm" href="'. route("usergroups.edit", [$this->id]). '">Edit</a>';
    }
}
