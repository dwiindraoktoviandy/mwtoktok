<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductInactive extends Model
{
    protected $table = 'product_inactive';

    protected $guarded = [];

    public function product()
    {
    	return $this->belongsTo('App\Models\Product', 'sku', 'sku');
    }
}
