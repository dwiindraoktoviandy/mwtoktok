<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantSetup extends Model
{
    protected $table = 'merchant_setup';

    protected $guarded = [];

    public static function getActiveMerchant()
    {
    	return \App\Models\MerchantSetup::first();
    }
}
