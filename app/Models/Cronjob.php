<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cronjob extends Model
{
    protected $guarded = [];

    public function vendor()
    {
    	# code...
    	return $this->belongsTo("App\Models\Vendor");
    }

    public function cronjoblog()
    {
    	# code...
    	return $this->hasMany("App\Models\CronjobLog");
    }
}
