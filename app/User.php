<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','username', 'usergroup_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['action'];

    public function getActionAttribute($value='')
    {
        return '<a class="btn btn-complete btn-sm" href="'. route("user.edit", [$this->id]). '">Edit</a>';
    }

    public function usergroup($value='')
    {
        # code...
        return $this->belongsTo('\App\Models\UserGroup');
    }
}
