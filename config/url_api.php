<?php

return [
	'url_idmarco' => env('URL_IDMARCO', 'http://imostaging.idmarco.com/'),
	'client_id_idmarco' => env('CLIENT_ID_IDMARCO', 'kioskuser'),
	'client_secret_idmarco' => env('CLIENT_SECRET_IDMARCO', 'kioskuser'),

	'url_toktok' => env('URL_TOKTOK', 'http://18.136.34.206/'),
	'client_id_toktok' => env('CLIENT_ID_TOKTOK', 'kioskuser'),
	'client_secret_toktok' => env('CLIENT_SECRET_TOKTOK', 'kioskuser'),
];