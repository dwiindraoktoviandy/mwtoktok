<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableVendorZone extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor_zones', function (Blueprint $table) {
            //
            $table->string('kode_sp')->nullable();
            $table->string('toktok_zone_ids')->nullable();
            $table->string('kecamatan')->nullable();
            $table->string('kelurahan')->nullable();
            $table->string('provinsi')->nullable();
            $table->string('kota')->nullable();
            $table->string('no_telepon')->nullable();
            $table->string('no_fax')->nullable();
            $table->string('status')->nullable();
            $table->string('bacd')->nullable();
            $table->string('pic_nama')->nullable();
            $table->string('pic_hp')->nullable();
            $table->string('pic_email')->nullable();
            $table->text('alamat')->nullable();
            $table->datetime('lastsyncvendor')->nullable();

        });

        Schema::table('toktok_zones', function (Blueprint $table) {
            //            
            $table->datetime('lastsyncimo_toktok')->nullable();            
        });

        Schema::table('vendor_zone_coverages', function (Blueprint $table) {
            //            
            $table->string('kelurahan_code')->nullable();
            $table->string('provinsi')->nullable();
            $table->string('kota')->nullable();            
            $table->string('kecamatan')->nullable();
            $table->string('kelurahan')->nullable();
            $table->string('kodepos')->nullable();
            $table->string('createdby')->nullable();
            $table->string('modifiedby')->nullable();
            $table->string('kode_sp')->nullable();
            $table->datetime('lastsyncvendor')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendor_zones', function (Blueprint $table) {
            //
        });
    }
}
