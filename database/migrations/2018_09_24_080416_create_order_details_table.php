<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->integer('product_frontend_id')->unsigned();
            $table->integer('qty')->default(0);
            $table->float('price', 15, 5)->default(0);
            $table->string('sku', 50)->nullable();
            $table->string('nama_fe', 150)->nullable();
            $table->string('uom', 10)->nullable();
            $table->string('remark')->nullable();
            $table->string('gross_amount')->nullable();
            $table->dateTime('fe_sync_date');
            $table->float('net_amount', 18, 4)->default(0);
            $table->float('discount_idm', 18, 4)->default(0);
            $table->float('discount_iap', 18, 4)->default(0);
            $table->float('disc_amount', 18, 4)->default(0);
            $table->float('vat_percent', 18, 4)->default(0);
            $table->dateTime('sync_date');
            $table->dateTime('last_mw_sync_date');
            $table->dateTime('last_fe_sync_date');
            $table->integer('ori_qty')->default(0);
            $table->integer('ori_price')->default(0);
            $table->integer('ori_discount')->default(0);
            $table->integer('ori_discount_idm')->default(0);
            $table->integer('ori_discount_iap')->default(0);
            $table->string('conversion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
