<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrderStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_status', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ordernofe', 30);
            $table->string('kode_sp', 20);
            $table->string('orderstat', 50);
            $table->datetime('mwtimestamp');
            $table->string('receiver', 60);
            $table->datetime('mw_statusdate');
            $table->datetime('syncdate');
            $table->datetime('lastmwsyncdate');
            $table->string('doctype', 50);
            $table->string('detail');
            $table->string('created_by');
            $table->string('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_status');
    }
}
