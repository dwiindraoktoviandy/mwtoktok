<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCronjobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cronjobs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id')->unsigned();
            $table->string('name')->default('');
            $table->string('description')->default('');

            $table->string('request_url')->default('');
            // $table->text('request_post')->default('');
            // $table->text('request_cookies')->default('');

            $table->tinyInteger('time_defined');
            $table->string('every_expression')->nullable();
            $table->string('custom_expression')->nullable();

            $table->string('param_minute', 100)->nullable();
            $table->string('param_hour', 100)->nullable();
            $table->string('param_day', 100)->nullable();
            $table->string('param_month', 100)->nullable();
            $table->string('param_week', 100)->nullable();

            $table->boolean('status')->default(false);
            $table->boolean('status_log')->default(false);
            
            $table->tinyInteger('status_notify')->default(0);
            $table->string('status_notify_email')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cronjobs');
    }
}
