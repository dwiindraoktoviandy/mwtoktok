<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeliveryToOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('deliveryname')->nullable();
            $table->text('deliveryaddress')->nullable();
            $table->string('deliveryrt', 5)->nullable();
            $table->string('deliveryrw', 5)->nullable();
            $table->string('deliveryprovinsi', 50)->nullable();
            $table->string('deliverykota', 100)->nullable();
            $table->string('deliverykecamatan', 50)->nullable();
            $table->string('deliverykelurahan', 50)->nullable();
            $table->string('deliveryphone', 50)->nullable();
            $table->string('kode_sp', 16)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
        });
    }
}
