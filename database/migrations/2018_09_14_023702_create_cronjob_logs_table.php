<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCronjobLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cronjob_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cronjob_id')->unsigned();
            $table->string('message')->nullable();
            $table->string('status')->nullable();
            $table->dateTime('last_sync');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cronjob_logs');
    }
}
