<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableReturnDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('return_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('return_id')->unsigned();
            $table->string('sku');
            $table->integer('qty');
            $table->string('uom')->nullable();
            $table->string('reason')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->integer('qty_return');
            $table->integer('qty_order');
            $table->integer('orderdetail_id');
            $table->float('unit_price_order', 10, 2)->nullable();
            $table->float('discount', 10, 2)->nullable();
            $table->float('amount_order', 10, 2)->nullable();
            $table->float('unit_price_nkr', 10, 2)->nullable();
            $table->float('amount_nkr', 10, 2)->nullable();
            $table->string('photo_url')->nullable();
            $table->integer('qty_nkr')->nullable();
            $table->string('ordernofe')->nullable();
            $table->text('notes_nkr')->nullable();
            $table->integer('qty_reshuffle')->nullable();
            $table->integer('rma_id_fe')->nullable();
            $table->datetime('lastsynctoktok')->nullable();
            $table->datetime('lastsyncvendor')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('return_detail');
    }
}
