<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldLastsynctoktokTablePaidsorevreq extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('paidsorevreq', function (Blueprint $table) {
            $table->datetime('lastsynctoktok')->nullable();
            $table->datetime('lastsyncvendor')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paidsorevreq', function (Blueprint $table) {
            //
        });
    }
}
