<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableReturn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('return', function (Blueprint $table) {
            $table->increments('id');
            $table->string('frontend_id');
            $table->integer('order_id'); 
            $table->integer('outlet_id');
            $table->integer('status_return')->nullable();
            $table->integer('status_request')->nullable();
            $table->string('status_mw')->nullable();
            $table->text('status_mw_detail')->nullable();
            $table->dateTime('mw_sync_date')->nullable();
            $table->dateTime('fe_sync_date');
            $table->dateTime('request_date');
            $table->dateTime('return_date_complete')->nullable();
            $table->text('note')->nullable();
            $table->text('desc')->nullable();
            $table->string('remark')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('retur_number');
            $table->string('order_number');
            $table->tinyInteger('status_nkr');
            $table->float('nkr_value');
            $table->string('photo_url')->nullable();
            $table->date('jadwal_resuffle')->nullable();
            $table->string('kode_sp', 25)->nullable();
            $table->integer('revision');
            $table->string('status_fe')->nullable();
            $table->string('status_mw_nkr')->nullable();
            $table->text('status_mw_nkr_detail')->nullable();
            $table->dateTime('mw_nkr_sync_date')->nullable();
            $table->integer('revision_nkr');
            $table->datetime('lastsynctoktok')->nullable();
            $table->datetime('lastsyncvendor')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('return');
    }
}
