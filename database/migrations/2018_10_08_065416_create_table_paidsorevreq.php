<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaidsorevreq extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paidsorevreq', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ordernofe', 30);
            $table->tinyInteger('status')->default(0)->nullable();
            $table->text('desc')->nullable();
            $table->text('data')->nullable();
            $table->string('message')->nullable();
            $table->string('rev_number')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paidsorevreq', function (Blueprint $table) {
            //
        });
    }
}
