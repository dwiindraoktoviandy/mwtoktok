<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductInactiveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_inactive', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sku');
            $table->integer('status_approve');
            $table->integer('inactive');
            $table->datetime('lastsyncvendor')->nullable();
            $table->datetime('lastsynctoktok')->nullable();
            $table->integer('needsyncvendor')->default(0);
            $table->integer('needsynctoktok')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
