<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('no_order');
            $table->integer('id_customer')->unsigned();
            $table->dateTime('order_date');
            $table->string('payment_type', 50)->nullable();
            $table->string('payment', 50)->nullable();
            $table->dateTime('payment_date');
            // $table->integer('product_id')->unsigned();
            $table->float('discount', 15, 5)->default(0);
            $table->integer('poin_redem');
            $table->float('shipment_fee', 15, 5)->default(0);
            $table->float('total', 15, 5)->default(0);
            $table->string('status_order', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
