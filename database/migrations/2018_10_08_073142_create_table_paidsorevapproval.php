<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaidsorevapproval extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paidsorevapproval', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('revreq_id')->unsigned();
            $table->tinyInteger('status')->default(0)->nullable();
            $table->text('followup')->nullable();
            $table->text('message')->nullable();
            $table->text('notes')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paidsorevapproval');
    }
}
