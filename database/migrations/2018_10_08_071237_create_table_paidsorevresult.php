<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaidsorevresult extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paidsorevresult', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('revreq_id')->unsigned();
            $table->string('sku')->nullable();
            $table->tinyInteger('status')->default(0)->nullable();
            $table->text('notes')->nullable();
            $table->text('message')->nullable();
            $table->text('desc')->nullable();
            $table->string('qty')->nullable();
            $table->string('qty_after')->nullable();
            $table->float('price', 10, 2)->default(0);
            $table->float('price_after', 10, 2)->default(0);
            $table->float('discamount', 18, 2)->default(0);
            $table->float('discamount_after', 18, 2)->default(0);
            $table->float('discidm', 18, 2)->default(0);
            $table->float('discidm_after', 18, 2)->default(0);
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paidsorevresult');
    }
}
