<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeQtyMinStockToVarcharForStocks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stocks', function (Blueprint $table) {
            \DB::select("ALTER TABLE `stocks` CHANGE `qty` `qty` INT(10) UNSIGNED NOT NULL DEFAULT 0");
            \DB::select("ALTER TABLE `stocks` CHANGE `min_stock` `min_stock` INT(10) UNSIGNED NOT NULL DEFAULT 0");
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stocks', function (Blueprint $table) {
            //
        });
    }
}
