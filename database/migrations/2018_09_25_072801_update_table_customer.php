<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->string('fe_id')->nullable();  
            $table->string('nip')->nullable();  
            $table->string('nama')->nullable();  
            $table->string('alamat')->nullable();  
            $table->string('provinsi')->nullable();  
            $table->string('kota')->nullable();  
            $table->string('kecamatan')->nullable();  
            $table->string('kelurahan')->nullable();  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
