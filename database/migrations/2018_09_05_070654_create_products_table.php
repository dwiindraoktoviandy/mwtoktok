<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sku')->nullable();
            $table->string('product_name')->nullable();
            $table->integer('vendor_id')->unsigned();
            $table->string('short_description')->nullable();
            $table->string('desc')->nullable();
            $table->string('merk')->nullable();
            $table->string('unit')->nullable();
            $table->integer('multiple_unit')->default(0);
            $table->float('conversion', 8, 2)->default(0);
            $table->float('package_long', 8, 2)->default(0);
            $table->float('package_height', 8, 2)->default(0);
            $table->float('dimension_long', 8, 2)->default(0);
            $table->float('dimension_width', 8, 2)->default(0);
            $table->float('dimension_height', 8, 2)->default(0);
            $table->string('nett')->nullable();
            $table->text('sertifikasi')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
