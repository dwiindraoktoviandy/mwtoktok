<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//register
// Route::get('/home', 'HomeController@index')->name('home');
// Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
// Route::post('register', 'Auth\RegisterController@register');
//endregister

//login
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');

//cronjob
Route::get('execution/{id}', 'Cronjob\CronjobController@execution')->name('execution');
Route::prefix('cronjob_list')->name('cronjob_list.')->group(function () {
    // $name = 'cronjob_list.';
    // Route::get('getdataproduct', 'Api\Imo\ImoController@getdataproduct')->name('getdataproduct');
    Route::get('getstokcpointvendor', 'Zone\ZoneController@getstokcpointvendor')->name('getstokcpointvendor');
    Route::get('getstockpointcoveragevendor', 'Zone\ZoneController@getstockpointcoveragevendor')->name('getstockpointcoveragevendor');
    
    // Route::get('getdatavendor', 'Api\Imo\ImoController@getdatavendor')->name('vendor.getdatavendor')
    Route::get('getdatavendor', 'Api\Imo\ImoController@getdatavendor')->name('getdatavendor');
    Route::get('getdatacustomer', 'Api\Imo\ImoController@getdatacustomer')->name('getdatacustomer');
    Route::get('getdataorder', 'Api\Imo\ImoController@getdataorder')->name('getdataorder');
    Route::get('postdatacustomer', 'Api\Imo\ImoController@postdatacustomer')->name('postdatacustomer');
    Route::get('postdataproduct', 'Api\Imo\ImoController@postdataproduct')->name('postdataproduct');
    Route::get('postdataprice', 'Api\Imo\ImoController@postdataprice')->name('postdataprice');
    Route::get('postdatastock', 'Api\Imo\ImoController@postdatastock')->name('postdatastock');
    Route::get('postdataorder', 'Api\Imo\ImoController@postdataorder')->name('postdataorder');
    Route::get('getdataproduct', 'Api\Imo\ImoController@getdataproduct')->name('getdataproduct');
    Route::get('getdataproductinactive', 'Api\Imo\ImoController@getProductInactive')->name('getdataproductinactive');
    Route::get('postdataproductinactive', 'Api\Imo\ImoController@postProductInactive')->name('postdataproductinactive');
    Route::get('getdataproductbtn', 'Api\Imo\ImoController@getdataproductbtn')->name('getdataproductbtn');
    Route::get('getdataprice', 'Api\Imo\ImoController@getdataprice')->name('getdataprice');
    Route::get('getdatastock', 'Api\Imo\ImoController@getdatastock')->name('getdatastock');
    Route::get('getmwstatus', 'Api\Imo\ImoController@getmwstatus')->name('getmwstatus');
    Route::get('postmwstatus', 'Api\Imo\ImoController@postmwstatus')->name('postmwstatus');
    Route::get('getorderstatus', 'Api\Imo\ImoController@getorderstatus')->name('getorderstatus');
    Route::get('postorderstatus', 'Api\Imo\ImoController@postorderstatus')->name('postorderstatus');
    Route::get('getordertracking', 'Api\Imo\ImoController@getordertracking')->name('getordertracking');
    Route::get('postordertracking', 'Api\Imo\ImoController@postordertracking')->name('postordertracking');
    Route::get('Createrevision', 'Api\Imo\ImoController@Createrevision')->name('createrevision');
    Route::get('pushrevision', 'Api\Imo\ImoController@pushRevision')->name('pushrevision');
    Route::get('get_orderresuffle', 'Api\Imo\ImoController@get_orderresuffle')->name('get_orderresuffle');
    Route::get('clearlog', 'Api\Imo\ImoController@clearlog')->name('clearlog');
});
//cronjob


//endlogin
Route::middleware(['acl'])->group(function () {

Route::prefix('merchant')->group(function() {
    Route::get('/', 'Merchant\MerchantSetupController@show')->name('merchant.show');
    Route::post('/store', 'Merchant\MerchantSetupController@store')->name('merchant.store');
    Route::put('/{id}', 'Merchant\MerchantSetupController@update')->name('merchant.update');
});

//acl
Route::prefix('user')->group(function () {
    Route::get('/', 'User\UserController@index')->name('user.index');
    Route::post('/store', 'User\UserController@store')->name('user.store');
    Route::get('{id}/edit', 'User\UserController@edit')->name('user.edit');
    Route::put('{id}', 'User\UserController@update')->name('user.update');
    Route::get('/user-list', 'User\UserHelper@userlist')->name('user.user-list');       
});

Route::prefix('usergroups')->group(function () {
    Route::get('/', 'User\UserGroupsController@index')->name('usergroups.index');
    Route::post('/store', 'User\UserGroupsController@store')->name('usergroups.store');
    Route::get('{id}/edit', 'User\UserGroupsController@edit')->name('usergroups.edit');
    Route::put('{id}', 'User\UserGroupsController@update')->name('usergroups.update');
    Route::get('/data-list', 'User\UserGroupsHelper@datalist')->name('usergroups.usergroups-list');     
});
//endacl

Route::get('/', 'Cronjob\CronjobController@index')->name('index.customer');
// Route::get('/', function () {
//     return redirect()->route('cronjob.setup');
//     // return view('cronjob.index');
// });

// Auth::routes();

Route::get('/test', function(){
    $class = new \App\Http\Controllers\Vendor\VendorService;
    return $class->getstockpoint();
});

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('cronjob', 'Cronjob\CronjobController')->except([
    'destroy', 'show'
]);
Route::get('cronjob-setup', 'Cronjob\CronjobController@setup')->name('cronjob.setup');

Route::prefix('imoidmarco')->name('api.imo.')->group(function () {
    Route::get('getdataproduct', 'Api\Imo\ImoController@getdataproduct')->name('getdataproduct');
    Route::get('getdatavendor', 'Api\Imo\ImoController@getdatavendor')->name('getdatavendor');
    Route::get('getdatacustomerbtn', 'Api\Imo\ImoController@getdatacustomerbtn')->name('getdatacustomerbtn');
    // Route::get('getdatavendorbtn', 'Api\Imo\ImoController@getdatavendorbtn')->name('getdatavendorbtn');
});


Route::prefix('master')->name('master.')->group(function () {
	Route::get('vendor', 'Vendor\VendorController@index')->name('vendor');
	Route::get('vendor-update/{id}', 'Vendor\VendorController@update')->name('vendor.update');
	Route::post('vendor-store/{id}', 'Vendor\VendorController@simpan')->name('vendor.store');
	
	Route::get('ajaxvendor', 'Vendor\VendorController@ajaxvendor')->name('ajaxvendor.datatables');
});

Route::prefix('transaction')->name('transaction.')->group(function () {
    Route::get('product', 'Product\ProductController@index')->name('product');
    Route::get('inventory', 'Inventory\InventoryController@index')->name('inventory');
    Route::get('zones', 'Zone\ZoneController@index')->name('zone.index');
    Route::get('price', 'Price\PriceController@index')->name('price.index');
});

Route::get('apilist', 'Api\APIController@index')->name('api.list');
Route::get('addnewapi', 'Api\APIController@create')->name('api.create');

Route::prefix('datatables')->name('datatables.')->group(function () {
    Route::get('getstockpoint', 'Zone\ZoneController@getstockpoint')->name('getstockpoint');
});


Route::get('ordertoktok', 'Order\OrderController@indextoktok')->name('order.index');

Route::get('customer', 'Customer\CustomerController@index')->name('index.customer');
Route::get('btngetstockpointvendor', 'Zone\ZoneController@btngetstockpointvendor')->name('btngetstockpointvendor');
Route::get('btngetstockpointcoveragevendor', 'Zone\ZoneController@btngetstockpointcoveragevendor')->name('btngetstockpointcoveragevendor');

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
});
// Auth::routes();
