<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?> ">
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <meta name="csrf-token" content=" <?php echo e(csrf_token()); ?>" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    
    <title><?php echo e(config('app.name')); ?></title>

    <link rel="apple-touch-icon" href="<?php echo e(asset('template/pages/ico/60.png')); ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo e(asset('template/pages/ico/76.png')); ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo e(asset('template/pages/ico/120.png')); ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo e(asset('template/pages/ico/152.png')); ?>">

    <link rel="icon" type="image/x-icon" href="<?php echo e(asset('template/pages/favicon.ico')); ?>f" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />

    <link href="<?php echo e(asset('template/pages/assets/plugins/pace/pace-theme-flash.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('template/pages/assets/plugins/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('template/pages/assets/plugins/font-awesome/css/font-awesome.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('template/pages/assets/plugins/jquery-scrollbar/jquery.scrollbar.css')); ?>" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo e(asset('template/pages/assets/plugins/select2/css/select2.min.css')); ?>" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo e(asset('template/pages/assets/plugins/switchery/css/switchery.min.css')); ?>" rel="stylesheet" type="text/css" media="screen" />
  
    <?php echo $__env->yieldPushContent('style'); ?>

    <link href="<?php echo e(asset('template/pages/pages/css/pages-icons.css')); ?>" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="<?php echo e(asset('template/pages/pages/css/pages.css')); ?>" rel="stylesheet" type="text/css" />
  </head>
  <body class="fixed-header dashboard">
    <!-- BEGIN SIDEBPANEL-->
    <nav class="page-sidebar" data-pages="sidebar">
      <!-- BEGIN SIDEBAR MENU TOP TRAY CONTENT-->
      <div class="sidebar-overlay-slide from-top" id="appMenu">
        <div class="row">
          <div class="col-xs-6 no-padding">
            <a href="#" class="p-l-40"><img src="<?php echo e(asset('template/pages/assets/img/demo/social_app.svg')); ?>" alt="socail">
            </a>
          </div>
          <div class="col-xs-6 no-padding">
            <a href="#" class="p-l-10"><img src="<?php echo e(asset('template/pages/assets/img/demo/email_app.svg')); ?>" alt="socail">
            </a>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-6 m-t-20 no-padding">
            <a href="#" class="p-l-40"><img src="<?php echo e(asset('template/pages/assets/img/demo/calendar_app.svg')); ?>" alt="socail">
            </a>
          </div>
          <div class="col-xs-6 m-t-20 no-padding">
            <a href="#" class="p-l-10"><img src="<?php echo e(asset('template/pages/assets/img/demo/add_more.svg')); ?>" alt="socail">
            </a>
          </div>
        </div>
      </div>
      <!-- END SIDEBAR MENU TOP TRAY CONTENT-->
      <!-- BEGIN SIDEBAR MENU HEADER-->
      <div class="sidebar-header">
        <img src="<?php echo e(asset('template/pages/assets/img/logo.jpeg')); ?>" alt="logo" class="brand" data-src="<?php echo e(asset('template/pages/assets/img/logo.jpeg')); ?>" data-src-retina="<?php echo e(asset('template/pages/assets/img/logo_white_2x.png')); ?>" width="120" height="40">
        <div class="sidebar-header-controls">
          <button type="button" class="btn btn-xs sidebar-slide-toggle btn-link m-l-20 hidden-md-down" data-pages-toggle="#appMenu"><i class="fa fa-angle-down fs-16"></i>
          </button>
          <button type="button" class="btn btn-link hidden-md-down" data-toggle-pin="sidebar"><i class="fa fs-12"></i>
          </button>
        </div>
      </div>
      <!-- END SIDEBAR MENU HEADER-->
      <!-- START SIDEBAR MENU -->
      <div class="sidebar-menu">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
        <ul class="menu-items">
          <li class="m-t-30 ">
            <a href="<?php echo e(url('/')); ?>" class="title">
              <span class="title">Dashboard</span>
            </a>
            <span class="bg-success icon-thumbnail"><i class="pg-home"></i></span>
          </li>
      
          <li>
            <a href="javascript:;"><span class="title">Data PNS</span>
            <span class=" arrow"></span></a>
            <span class="icon-thumbnail"><i class="pg-calender"></i></span>
            <ul class="sub-menu">
              <li class="">
                <a href="javascript:;"><span class="title">Master</span>
                <span class=" arrow"></span></a>
                <span class="icon-thumbnail">m</span>
                <ul class="sub-menu">
                  <li>
                    <a href="<?php echo e(url('golongan')); ?>">Master Golongan</a>
                  </li>
                  <li>
                    <a href="<?php echo e(url('instansi')); ?>">Master Instansi</a>
                  </li>
                  <li>
                    <a href="<?php echo e(url('jenis_instansi')); ?>">Master Jenis Instansi</a>
                  </li>             
                </ul>
              </li>
              <li class="">
                <a href="javascript:;"><span class="title">Transaction</span>
                <span class=" arrow"></span></a>
                <span class="icon-thumbnail">t</span>
                <ul class="sub-menu">
                  <li>
                    <a href="<?php echo e(url('pnslist')); ?>">PNS List</a>
                  </li>
                 <!--  <li>
                    <a href="<?php echo e(url('pnsinformation')); ?>">PNS Information</a>
                  </li> -->
                </ul>
                
              </li>              
              <li class="">
                <a href="#" target="_blank">Report</a>
                <span class="icon-thumbnail">r</span>
              </li>
            </ul>
          </li>
  
          <li>
            <a href="javascript:;"><span class="title">System</span>
            <span class=" arrow"></span></a>
            <span class="icon-thumbnail"><i class="fa fa-user"></i></span>
            <ul class="sub-menu">
              
              <li class="">
                <a href="<?php echo e(url('usergroups')); ?>">User Group</a>
              </li>
             
              <li class="">
                <a href="<?php echo e(url('user')); ?>">User</a> 
              </li>
             
            </ul>
          </li>

        </ul>
        <div class="clearfix"></div>
      </div>
      <!-- END SIDEBAR MENU -->
    </nav>
    <!-- END SIDEBAR -->
    <!-- END SIDEBPANEL-->
  
    <?php echo $__env->yieldContent('content'); ?> 

    <!-- START OVERLAY -->
    <div class="overlay hide" data-pages="search">
      <!-- BEGIN Overlay Content !-->
      <div class="overlay-content has-results m-t-20">
        <!-- BEGIN Overlay Header !-->
        <div class="container-fluid">
          <!-- BEGIN Overlay Logo !-->
          <img class="overlay-brand" src="<?php echo e(asset('template/pages/assets/img/logo.jpeg')); ?>" alt="logo" data-src="<?php echo e(asset('template/pages/assets/img/logo.jpeg')); ?>" data-src-retina="<?php echo e(asset('template/pages/assets/img/logo_2x.png')); ?>" width="78" height="22">
          <!-- END Overlay Logo !-->
          <!-- BEGIN Overlay Close !-->
          <a href="#" class="close-icon-light overlay-close text-black fs-16">
            <i class="pg-close"></i>
          </a>
          <!-- END Overlay Close !-->
        </div>
        <!-- END Overlay Header !-->
        <div class="container-fluid">
          <!-- BEGIN Overlay Controls !-->
          <input id="overlay-search" class="no-border overlay-search bg-transparent" placeholder="Search..." autocomplete="off" spellcheck="false">
          <br>
          <div class="inline-block">
            <div class="checkbox right">
              <input id="checkboxn" type="checkbox" value="1" checked="checked">
              <label for="checkboxn"><i class="fa fa-search"></i> Search within page</label>
            </div>
          </div>
          <div class="inline-block m-l-10">
            <p class="fs-13">Press enter to search</p>
          </div>
          <!-- END Overlay Controls !-->
        </div>
        <!-- BEGIN Overlay Search Results, This part is for demo purpose, you can add anything you like !-->
        <div class="container-fluid">
          <span>
                <strong>suggestions :</strong>
            </span>
          <span id="overlay-suggestions"></span>
          <br>
          <div class="search-results m-t-40">
            <p class="bold">Pages Search Results</p>
            <div class="row">
              <div class="col-md-6">
                <!-- BEGIN Search Result Item !-->
                <div class="">
                  <!-- BEGIN Search Result Item Thumbnail !-->
                  <div class="thumbnail-wrapper d48 circular bg-success text-white inline m-t-10">
                    <div>
                      <img width="50" height="50" src="<?php echo e(asset('template/pages/assets/img/profiles/avatar.jpg')); ?>" data-src="<?php echo e(asset('template/pages/assets/img/profiles/avatar.jpg')); ?>" data-src-retina="<?php echo e(asset('template/pages/assets/img/profiles/avatar2x.jpg')); ?>" alt="">
                    </div>
                  </div>
                  <!-- END Search Result Item Thumbnail !-->
                  <div class="p-l-10 inline p-t-5">
                    <h5 class="m-b-5"><span class="semi-bold result-name">ice cream</span> on pages</h5>
                    <p class="hint-text">via john smith</p>
                  </div>
                </div>
                <!-- END Search Result Item !-->
                <!-- BEGIN Search Result Item !-->
                <div class="">
                  <!-- BEGIN Search Result Item Thumbnail !-->
                  <div class="thumbnail-wrapper d48 circular bg-success text-white inline m-t-10">
                    <div>T</div>
                  </div>
                  <!-- END Search Result Item Thumbnail !-->
                  <div class="p-l-10 inline p-t-5">
                    <h5 class="m-b-5"><span class="semi-bold result-name">ice cream</span> related topics</h5>
                    <p class="hint-text">via pages</p>
                  </div>
                </div>
                <!-- END Search Result Item !-->
                <!-- BEGIN Search Result Item !-->
                <div class="">
                  <!-- BEGIN Search Result Item Thumbnail !-->
                  <div class="thumbnail-wrapper d48 circular bg-success text-white inline m-t-10">
                    <div><i class="fa fa-headphones large-text "></i>
                    </div>
                  </div>
                  <!-- END Search Result Item Thumbnail !-->
                  <div class="p-l-10 inline p-t-5">
                    <h5 class="m-b-5"><span class="semi-bold result-name">ice cream</span> music</h5>
                    <p class="hint-text">via pagesmix</p>
                  </div>
                </div>
                <!-- END Search Result Item !-->
              </div>
              <div class="col-md-6">
                <!-- BEGIN Search Result Item !-->
                <div class="">
                  <!-- BEGIN Search Result Item Thumbnail !-->
                  <div class="thumbnail-wrapper d48 circular bg-info text-white inline m-t-10">
                    <div><i class="fa fa-facebook large-text "></i>
                    </div>
                  </div>
                  <!-- END Search Result Item Thumbnail !-->
                  <div class="p-l-10 inline p-t-5">
                    <h5 class="m-b-5"><span class="semi-bold result-name">ice cream</span> on facebook</h5>
                    <p class="hint-text">via facebook</p>
                  </div>
                </div>
                <!-- END Search Result Item !-->
                <!-- BEGIN Search Result Item !-->
                <div class="">
                  <!-- BEGIN Search Result Item Thumbnail !-->
                  <div class="thumbnail-wrapper d48 circular bg-complete text-white inline m-t-10">
                    <div><i class="fa fa-twitter large-text "></i>
                    </div>
                  </div>
                  <!-- END Search Result Item Thumbnail !-->
                  <div class="p-l-10 inline p-t-5">
                    <h5 class="m-b-5">Tweats on<span class="semi-bold result-name"> ice cream</span></h5>
                    <p class="hint-text">via twitter</p>
                  </div>
                </div>
                <!-- END Search Result Item !-->
                <!-- BEGIN Search Result Item !-->
                <div class="">
                  <!-- BEGIN Search Result Item Thumbnail !-->
                  <div class="thumbnail-wrapper d48 circular text-white bg-danger inline m-t-10">
                    <div><i class="fa fa-google-plus large-text "></i>
                    </div>
                  </div>
                  <!-- END Search Result Item Thumbnail !-->
                  <div class="p-l-10 inline p-t-5">
                    <h5 class="m-b-5">Circles on<span class="semi-bold result-name"> ice cream</span></h5>
                    <p class="hint-text">via google plus</p>
                  </div>
                </div>
                <!-- END Search Result Item !-->
              </div>
            </div>
          </div>
        </div>
        <!-- END Overlay Search Results !-->
      </div>
      <!-- END Overlay Content !-->
    </div>
    <!-- END OVERLAY -->
    <!-- BEGIN VENDOR JS -->
    <script src="<?php echo e(asset('template/pages/assets/js/vue.js')); ?>" type="text/javascript"></script>

    <script src="<?php echo e(asset('template/pages/assets/plugins/pace/pace.min.js')); ?>" type="text/javascript"></script>
    <!-- <script src="<?php echo e(asset('template/pages/assets/plugins/jquery/jquery-1.11.1.min.js')); ?>" type="text/javascript"></script> -->
    <script src="https://code.jquery.com/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="<?php echo e(asset('template/pages/assets/plugins/modernizr.custom.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('template/pages/assets/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('template/pages/assets/plugins/tether/js/tether.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('template/pages/assets/plugins/bootstrap/js/bootstrap.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('template/pages/assets/plugins/jquery/jquery-easy.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('template/pages/assets/plugins/jquery-unveil/jquery.unveil.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('template/pages/assets/plugins/jquery-ios-list/jquery.ioslist.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('template/pages/assets/plugins/jquery-actual/jquery.actual.min.js')); ?>"></script>
    <script src="<?php echo e(asset('template/pages/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('template/pages/assets/plugins/select2/js/select2.full.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('template/pages/assets/plugins/classie/classie.js')); ?>"></script>
    <script src="<?php echo e(asset('template/pages/assets/plugins/switchery/js/switchery.min.js')); ?>" type="text/javascript"></script>


     <?php echo $__env->yieldPushContent('scripts'); ?>
    <!-- END VENDOR JS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="<?php echo e(asset('template/pages/pages/js/pages.min.js')); ?>"></script>
    <!-- END CORE TEMPLATE JS -->
    <!-- BEGIN PAGE LEVEL JS -->
    <!-- <script src="<?php echo e(asset('template/pages/assets/js/dashboard.js')); ?>"></script> -->
    <script src="<?php echo e(asset('template/pages/assets/js/scripts.js')); ?>" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS -->
  </body>
</html>