@extends('layouts.main')

@push('style')
<link href="{{ asset('template/pages/assets/plugins/pace/pace-theme-flash.css ') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/bootstrap/css/bootstrap.min.css ') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/font-awesome/css/font-awesome.css ') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/jquery-scrollbar/jquery.scrollbar.css ') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset('template/pages/assets/plugins/select2/css/select2.min.css ') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset('template/pages/assets/plugins/switchery/css/switchery.min.css ') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset('template/pages/assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css ') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css ') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/datatables-responsive/css/datatables.responsive.css ') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset('template/pages/pages/css/pages-icons.css ') }}" rel="stylesheet" type="text/css">
<link class="main-stylesheet" href="{{ asset('template/pages/pages/css/pages.css ') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')

<div class="jumbotron" data-pages="parallax">
  <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
    <div class="inner">
      <!-- START BREADCRUMB -->
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Master</a></li>
        <li class="breadcrumb-item active">Vendor</li>
      </ol>
      <!-- END BREADCRUMB -->      
    </div>
  </div>
</div>

<!-- START CONTAINER FLUID -->
<div class=" container-fluid   container-fixed-lg bg-white">
  <!-- START card -->
  <div class="card card-transparent">
    <div class="card-header  d-flex justify-content-between">
      <a class="btn btn-complete btn-cons" href="{{route('api.imo.getdatavendor')}}">Get Vendor</a>
 
      <div class="export-options-container"></div>
      <!-- <div class="clearfix"></div> -->
    </div>
    <div class="">
           @if (session('status'))
                    <div class="alert alert-success row">
                      <strong>Get Data Vendor Sukses </strong>
                      <br>
                      <button type="button" class="close" data-dismiss="alert">×</button>
                    </div>
                @endif           
                @if (session('kosong'))
                    <div class="alert alert-success row">
                      <strong>Data Vendor Sudah Yang Terbaru </strong>
                      <br>
                      <button type="button" class="close" data-dismiss="alert">×</button>
                    </div>
                @endif                
                @if (session('update'))
                    <div class="alert alert-success row">
                      <strong>Sukses Menambahkan! </strong>
                      <br>
                      <button type="button" class="close" data-dismiss="alert">×</button>
                    </div>
                @endif
    </div>
                    
    <div class="card-block">
      <table class="table"  id="vendorajax">
        <thead>
          <tr>
            <th style="text-align: center;">Vendor Code</th>
            <th style="text-align: center;">Vendor Name</th>
            <th style="text-align: center;">Server URL</th>
          </tr>
        </thead>
        <tbody>
          @foreach($vendors as $vendor)
          <tr>
            <th style="font-weight: 500;text-align: center;">{{$vendor->vendor_id}}</th>
            <th style="font-weight: 500;text-align: center;"><a href="{{route('master.vendor.update', $vendor->id)}}">{{$vendor->vendor_name}}</a></th>
            <th style="font-weight: 500;text-align: center;">{{$vendor->server_url}}</th>
          </tr>
          @endforeach
        </tbody>
        
      </table>
    </div>
  </div>
  <!-- END card -->
</div>
<!-- END CONTAINER FLUID -->

@endsection

@push('scripts')


<!-- BEGIN VENDOR JS -->
<script src="{{ asset('template/pages/assets/plugins/pace/pace.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery/jquery-1.11.1.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/modernizr.custom.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-ui/jquery-ui.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/tether/js/tether.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/bootstrap/js/bootstrap.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery/jquery-easy.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-unveil/jquery.unveil.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-ios-list/jquery.ioslist.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-actual/jquery.actual.min.js ') }}"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js ') }}"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/select2/js/select2.full.min.js ') }}"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/classie/classie.js ') }}"></script>
<script src="{{ asset('template/pages/assets/plugins/switchery/js/switchery.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js ') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/datatables-responsive/js/datatables.responsive.js ') }}"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/datatables-responsive/js/lodash.min.js ') }}"></script>
<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="{{ asset('template/pages/pages/js/pages.min.js ') }}"></script>
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL JS -->
<script src="{{ asset('template/pages/assets/js/datatables.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/js/scripts.js ') }}" type="text/javascript"></script>
<!-- <script type="text/javascript">

  var table = $('#vendorajax').DataTable({
    serverSide: true,
    processing: true,
    ajax: "{{ route('master.ajaxvendor.datatables') }}",
    columns: [
        { name: 'vendor_id' },
        { name: 'vendor_name' },       
        { name: 'server_url' },
    ],
});
$('#vendorajax').on('click', 'tbody tr', function () {
                var id = table.row( this ).data();
                // var link = text.link("{{route('master.vendor.update', 'id[0]')}}");
                var link = location.href='{{route("master.vendor.update",'+id[0]+')}}';
                // console.log(id[0])
                console.log(link)
            });
</script> -->
<!-- END PAGE LEVEL JS -->
@endpush