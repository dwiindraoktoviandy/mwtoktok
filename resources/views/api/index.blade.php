@extends('layouts.main')

@push('style')
<link href="{{ asset('template/pages/assets/plugins/pace/pace-theme-flash.css ') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/bootstrap/css/bootstrap.min.css ') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/font-awesome/css/font-awesome.css ') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/jquery-scrollbar/jquery.scrollbar.css ') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset('template/pages/assets/plugins/select2/css/select2.min.css ') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset('template/pages/assets/plugins/switchery/css/switchery.min.css ') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset('template/pages/assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css ') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css ') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/datatables-responsive/css/datatables.responsive.css ') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset('template/pages/pages/css/pages-icons.css ') }}" rel="stylesheet" type="text/css">
<link class="main-stylesheet" href="{{ asset('template/pages/pages/css/pages.css ') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
  .tengah{
    text-align: center;
  }
  .notbold{
    font-weight: 500;
  }
</style>
@endpush

@section('content')

<div class="jumbotron" data-pages="parallax">
  <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
    <div class="inner">
      <!-- START BREADCRUMB -->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item active">API List</li>
      </ol>
      <!-- END BREADCRUMB -->      
    </div>
  </div>
</div>

<!-- START CONTAINER FLUID -->
<div class=" container-fluid   container-fixed-lg bg-white">
  <!-- START card -->
  <div class="card card-transparent">
    	<div class="col-sm-12" style="text-align: center;"><br><H3>API List</H3></div>
    <div class="card-header  d-flex justify-content-between">
      <a class="btn btn-complete btn-cons" href="#">Add New API</a>
 
      <div class="export-options-container"></div>
      <!-- <div class="clearfix"></div> -->
    </div>
    <div class="">
           @if (session('status'))
                    <div class="alert alert-success row">
                      <strong>Get Data Vendor Sukses </strong>
                      <br>
                      <button type="button" class="close" data-dismiss="alert">×</button>
                    </div>
                @endif           
                @if (session('kosong'))
                    <div class="alert alert-success row">
                      <strong>Data Vendor Sudah Yang Terbaru </strong>
                      <br>
                      <button type="button" class="close" data-dismiss="alert">×</button>
                    </div>
                @endif                
                @if (session('update'))
                    <div class="alert alert-success row">
                      <strong>Sukses Menambahkan! </strong>
                      <br>
                      <button type="button" class="close" data-dismiss="alert">×</button>
                    </div>
                @endif
    </div>
    <div class="col-sm-3">
        <select class="full-width" data-init-plugin="select2" name="vendor" id="vendor">
                <option>Choose Vendor</option>
                <option value=""></option>
         </select>
    </div>
                    
    <div class="card-block">
      <table class="table"  id="vendorajax">
        <thead>
          <tr>
            <th class="tengah">Vendor Name</th>
            <th class="tengah">API Name</th>
            <th class="tengah">API Url</th>
            <th class="tengah">Url</th>
            <th class="tengah">Status</th>
            <th class="tengah">Created Date</th>
            <th class="tengah">Update Date</th>
          </tr>
        </thead>
        <tbody>

        </tbody>
        
      </table>
    </div>
  </div>
  <!-- END card -->
</div>
<!-- END CONTAINER FLUID -->

@endsection

@push('scripts')


<!-- BEGIN VENDOR JS -->
<script src="{{ asset('template/pages/assets/plugins/pace/pace.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery/jquery-1.11.1.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/modernizr.custom.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-ui/jquery-ui.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/tether/js/tether.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/bootstrap/js/bootstrap.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery/jquery-easy.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-unveil/jquery.unveil.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-ios-list/jquery.ioslist.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-actual/jquery.actual.min.js ') }}"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js ') }}"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/select2/js/select2.full.min.js ') }}"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/classie/classie.js ') }}"></script>
<script src="{{ asset('template/pages/assets/plugins/switchery/js/switchery.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js ') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/datatables-responsive/js/datatables.responsive.js ') }}"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/datatables-responsive/js/lodash.min.js ') }}"></script>
<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="{{ asset('template/pages/pages/js/pages.min.js ') }}"></script>
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL JS -->
<script src="{{ asset('template/pages/assets/js/datatables.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/js/scripts.js ') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL JS -->
@endpush