@extends('layouts.main')

@push('style')
<link href="{{ asset('template/pages/assets/plugins/pace/pace-theme-flash.css ') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/bootstrap/css/bootstrap.min.css ') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/font-awesome/css/font-awesome.css ') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/jquery-scrollbar/jquery.scrollbar.css ') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset('template/pages/assets/plugins/select2/css/select2.min.css ') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset('template/pages/assets/plugins/switchery/css/switchery.min.css ') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset('template/pages/assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css ') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css ') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/datatables-responsive/css/datatables.responsive.css ') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset('template/pages/pages/css/pages-icons.css ') }}" rel="stylesheet" type="text/css">
<link class="main-stylesheet" href="{{ asset('template/pages/pages/css/pages.css ') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
  .tengah{
    text-align: center;
  }
  .notbold{
    font-weight: 500;
  }
</style>
@endpush

@section('content')

<div class="jumbotron" data-pages="parallax">
  <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
    <div class="inner">
      <!-- START BREADCRUMB -->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item active">Merchant Setup</li>
      </ol>
      <!-- END BREADCRUMB -->      
    </div>
  </div>
</div>

<!-- START CONTAINER FLUID -->
<div class=" container-fluid   container-fixed-lg bg-white">
  <!-- START card -->
  <div class="card card-transparent">
    	<div class="col-sm-12" style="text-align: center;"><br><H3>Merchant Setup</H3></div>
    <div class="card-header  d-flex justify-content-between">
 
      <div class="export-options-container"></div>
      <!-- <div class="clearfix"></div> -->
    </div>
    <div class="">
      @if (session('success'))
        <div class="alert alert-success row">
          <strong>Merchant Setup Has Been Saved</strong>
          <br>
          <button type="button" class="close" data-dismiss="alert">×</button>
        </div>
      @endif
    </div>
                    
    <div class="card-block">
      {!!$data ? '<form id="form-personal" role="form" autocomplete="off" method="POST" action="'.route('merchant.update', [$data->id]).'">'. method_field('PUT') : '<form id="form-personal" role="form" autocomplete="off" method="POST" action="'.route('merchant.store').'">'!!}
        
        {{ csrf_field() }}
          <div class="row">
            <div class="col-md-12">
              <div class="form-group form-group-default">
                <label>Name</label>
                <input id="name" type="text" class="form-control" name="name" value="{{$data ? $data->name : old('name')}}" required autofocus>
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
              </div>
            </div>
          </div>
          
          <div class="row">
            <div class="col-md-12">
              <div class="form-group form-group-default">
                <label>Merchant ID</label>
                <input id="merchant_id" type="text" class="form-control" name="merchant_id" value="{{$data ? $data->merchant_id : old('merchant_id')}}" required autofocus>
                @if ($errors->has('merchant_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('merchant_id') }}</strong>
                    </span>
                @endif
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="form-group form-group-default">
                <label>Customer Group Code</label>
                <input id="customergroup_code" type="text" class="form-control" name="customergroup_code" value="{{$data ? $data->customergroup_code : old('customergroup_code')}}" required autofocus>
                @if ($errors->has('customergroup_code'))
                    <span class="help-block">
                        <strong>{{ $errors->first('customergroup_code') }}</strong>
                    </span>
                @endif
              </div>
            </div>
          </div>   

          <div class="row">
            <div class="col-md-12">
              <div class="form-group form-group-default">
                <label>Prefix</label>
                <input id="prefix" type="text" class="form-control" name="prefix" value="{{$data ? $data->prefix : old('prefix')}}" required autofocus>
                @if ($errors->has('prefix'))
                    <span class="help-block">
                        <strong>{{ $errors->first('prefix') }}</strong>
                    </span>
                @endif
              </div>
            </div>
          </div>   
          
          <div class="clearfix"></div>
          {!!$data ? '<button class="btn btn-complete" type="submit">Save</button>&nbsp;<a class="btn btn-complete" href="'.route("merchant.show").'">Cancel</a>' : '<button class="btn btn-complete" type="submit">Save</button>'!!}
          
        </form>
    </div>
  </div>
  <!-- END card -->
</div>
<!-- END CONTAINER FLUID -->

@endsection

@push('scripts')


<!-- BEGIN VENDOR JS -->
<script src="{{ asset('template/pages/assets/plugins/pace/pace.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery/jquery-1.11.1.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/modernizr.custom.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-ui/jquery-ui.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/tether/js/tether.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/bootstrap/js/bootstrap.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery/jquery-easy.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-unveil/jquery.unveil.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-ios-list/jquery.ioslist.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-actual/jquery.actual.min.js ') }}"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js ') }}"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/select2/js/select2.full.min.js ') }}"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/classie/classie.js ') }}"></script>
<script src="{{ asset('template/pages/assets/plugins/switchery/js/switchery.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js ') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/datatables-responsive/js/datatables.responsive.js ') }}"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/datatables-responsive/js/lodash.min.js ') }}"></script>
<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="{{ asset('template/pages/pages/js/pages.min.js ') }}"></script>
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL JS -->
<script src="{{ asset('template/pages/assets/js/datatables.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/js/scripts.js ') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL JS -->
@endpush