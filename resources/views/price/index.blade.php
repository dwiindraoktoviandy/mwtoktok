@extends('layouts.main')

@push('style')
<link href="{{ asset('template/pages/assets/plugins/pace/pace-theme-flash.css ') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/bootstrap/css/bootstrap.min.css ') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/font-awesome/css/font-awesome.css ') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/jquery-scrollbar/jquery.scrollbar.css ') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset('template/pages/assets/plugins/select2/css/select2.min.css ') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset('template/pages/assets/plugins/switchery/css/switchery.min.css ') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset('template/pages/assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css ') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css ') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/datatables-responsive/css/datatables.responsive.css ') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset('template/pages/pages/css/pages-icons.css ') }}" rel="stylesheet" type="text/css">
<link class="main-stylesheet" href="{{ asset('template/pages/pages/css/pages.css ') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
.tengah {
    text-align: center;
}

.notbold {
    font-weight: 500;
}
</style>
@endpush

@section('content')
<div class="jumbotron" data-pages="parallax">
    <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
        <div class="inner">
            <!-- START BREADCRUMB -->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Product</li>
                <li class="breadcrumb-item">Transaction</li>
                <li class="breadcrumb-item active">Inventory</li>
            </ol>
            <!-- END BREADCRUMB -->
        </div>
    </div>
</div>
<!-- START CONTAINER FLUID -->
<div class=" container-fluid container-fixed-lg bg-white">
    <!-- START card -->
    <div class="card card-transparent">
        <div class="col-sm-12" style="text-align: center;"><br><H3>Price</H3></div>
        <div class="card-header d-flex justify-content-between">
          <div class="row">
            
          </div>
        </div>
        <div class="card-body">
            <table class="table" style="width:100%" id="pricetable">
                <thead>
                    <tr>
                        <th class="tengah">Vendor Price</th>
                        <th class="tengah">Valid On</th>
                        <th class="tengah">Last Sync TokTok</th>
                        <th class="tengah">Last Sync Vendor</th>
                        <th class="tengah">SKU</th>
                        <th class="tengah">Kode SP</th>
                        <th class="tengah">Created At</th>
                        <th class="tengah">Updated At</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <!-- END card -->
</div>
<!-- END CONTAINER FLUID -->
@endsection

@push('scripts')
<!-- BEGIN VENDOR JS -->
<script src="{{ asset('template/pages/assets/plugins/pace/pace.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery/jquery-1.11.1.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/modernizr.custom.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-ui/jquery-ui.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/tether/js/tether.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/bootstrap/js/bootstrap.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery/jquery-easy.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-unveil/jquery.unveil.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-ios-list/jquery.ioslist.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-actual/jquery.actual.min.js ') }}"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js ') }}"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/select2/js/select2.full.min.js ') }}"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/classie/classie.js ') }}"></script>
<script src="{{ asset('template/pages/assets/plugins/switchery/js/switchery.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js ') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/datatables-responsive/js/datatables.responsive.js ') }}"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/datatables-responsive/js/lodash.min.js ') }}"></script>
<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="{{ asset('template/pages/pages/js/pages.min.js ') }}"></script>
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL JS -->
<script src="{{ asset('template/pages/assets/js/datatables.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/js/scripts.js ') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL JS -->
<script>
    $(document).ready(function() {
        $('#pricetable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route("transaction.price.index") }}',
            columns: [
                { data: 'vendor_price', name: 'vendor_price' },
                { data: 'valid_on', name: 'valid_on' },
                { data: 'lastsynctoktok', name: 'lastsynctoktok' },
                { data: 'lastsyncvendor', name: 'lastsyncvendor' },
                { data: 'sku', name: 'sku' },
                { data: 'kode_sp', name: 'kode_sp' },
                { data: 'created_at', name: 'created_at' },
                { data: 'updated_at', name: 'updated_at' }
            ],
            columnDefs: [
                
            ],
            order: [[7, 'desc']]
        });
    })
</script>
@endpush    