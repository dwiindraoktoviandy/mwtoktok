@extends('layouts.main')

@push('style')
<link href="{{ asset('template/pages/assets/plugins/pace/pace-theme-flash.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/font-awesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/jquery-scrollbar/jquery.scrollbar.css') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset('template/pages/assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset('template/pages/assets/plugins/switchery/css/switchery.min.css') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset('template/pages/assets/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/bootstrap-tag/bootstrap-tagsinput.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/dropzone/css/dropzone.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/bootstrap-datepicker/css/datepicker3.css') }}" rel="stylesheet" type="text/css" media="screen">
<link href="{{ asset('template/pages/assets/plugins/summernote/css/summernote.css') }}" rel="stylesheet" type="text/css" media="screen">
<link href="{{ asset('template/pages/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet" type="text/css" media="screen">
<link href="{{ asset('template/pages/assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet" type="text/css" media="screen">
<link href="{{ asset('template/pages/pages/css/pages-icons.css') }}" rel="stylesheet" type="text/css">
<link class="main-stylesheet" href="{{ asset('template/pages/pages/css/pages.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" />
<style type="text/css">
  td{
    text-align: center;
  }
  th{
    text-align: center;
  }
</style>
@endpush

@section('content')

<div class="jumbotron" data-pages="parallax">
  <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
    <div class="inner">
      <!-- START BREADCRUMB -->
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item"><a href="{{route("cronjob.index")}}">Cronjob</a></li>
        <li class="breadcrumb-item active">Cronjob setup</li>
      </ol>
      <!-- END BREADCRUMB -->      
    </div>
  </div>
</div>

<!-- START CONTAINER FLUID -->
<div class=" container-fluid   container-fixed-lg">
  <div class="row">
    <div class="col-lg-6">
      <!-- START card -->
      <div class="card card-default">
        <div class="card-header ">
          <div><a href="" data-toggle="modal" data-target="#myModal" class="btn btn-complete pull-right">Log</a></div>
          <div class="card-title">
            Cronjob setup
          </div>
        </div>
        <div class="card-block">          
          <form class="" role="form" action="{{$url}}" method="POST">
            @csrf
            @if($method == 'PUT')
            @method('PUT')
            @endif
            @include('cronjob.form.modal.log')
            <div class="form-group form-group-default form-group-default-select2">
              <label class="">Vendor</label>              
              <select class="full-width" name="vendor_id" data-placeholder="Select Vendor" data-init-plugin="select2">
                <option value="{{$data->vendor_id ?? ""}}">{{$data->vendor->vendor_name ?? ""}}</option>
                @foreach(\App\Models\Vendor::get() as $row)
                <option value="{{$row->id}}">{{$row->vendor_name}}</option>
                @endforeach                
              </select>
            </div>
            <div class="form-group form-group-default">
              <label>Api Name</label>
              <input type="text" name="name" class="form-control" value="{{$data->name ?? ""}}">
            </div>
            <div class="form-group form-group-default form-group-default-select2">
              <label class="">Api Url</label>              
              <select class="full-width" name="request_url" data-placeholder="Select api url" data-init-plugin="select2">
                <option value="{{$data->request_url ?? ""}}">{{$data->request_url ?? ""}}</option>
                @for($i=0; $i < count($list_url_cront); $i++)
                <option value="{{url($list_url_cront[$i])}}">{{url($list_url_cront[$i])}}</option>
                @endfor
              </select>
            </div>            
            <div class="form-group form-group-default form-group-default-select2">
              <label class="">Execute every</label>              
              <select class="full-width" name="every_expression" data-placeholder="Select every" data-init-plugin="select2">
                <option value="{{$data->every_expression ?? ""}}">{{isset($data->every_expression) ? $everyexpressionvalues[$data->every_expression] : ""}}</option>
                @foreach($everyexpressionvalues as $key => $value)
                <option value="{{$key}}">{{$value}}</option>
                @endforeach
              </select>
            </div>             
            <div class="form-group form-group-default">
              <label>Status Log</label>
              <div class="radio radio-success">
                <input type="radio" value="1" name="status_log" id="log_active">
                <label for="log_active">Active</label>
                <input type="radio" value="0" name="status_log" id="log_disactive">
                <label for="log_disactive">Disactive</label>
              </div>
            </div>
            <div class="form-group form-group-default">
              <label>Status Cron</label>
              <div class="radio radio-success">
                <input type="radio" value="1" name="status" id="status_active">
                <label for="status_active">Active</label>
                <input type="radio" value="0" name="status" id="status_disactive">
                <label for="status_disactive">Disactive</label>
              </div>
            </div>
            <button class="btn btn-complete btn-cons" type="submit">Submit</button>
          </form>
        </div>
      </div>
      <!-- END card -->
    </div>    
  </div>
</div>
<!-- END CONTAINER FLUID -->



@endsection

@push('scripts')
<!-- BEGIN VENDOR JS -->
<script src="{{ asset('template/pages/assets/plugins/pace/pace.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/modernizr.custom.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/tether/js/tether.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery/jquery-easy.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-unveil/jquery.unveil.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-ios-list/jquery.ioslist.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-actual/jquery.actual.min.js') }}"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/select2/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/classie/classie.js') }}"></script>
<script src="{{ asset('template/pages/assets/plugins/switchery/js/switchery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/jquery-autonumeric/autoNumeric.js') }}"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/dropzone/dropzone.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/bootstrap-tag/bootstrap-tagsinput.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/jquery-inputmask/jquery.inputmask.min.js') }}"></script>
<script src="{{ asset('template/pages/assets/plugins/bootstrap-form-wizard/js/jquery.bootstrap.wizard.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/summernote/js/summernote.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('template/pages/assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('template/pages/assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js') }}"></script>
<script src="{{ asset('template/pages/assets/plugins/bootstrap-typehead/typeahead.bundle.min.js') }}"></script>
<script src="{{ asset('template/pages/assets/plugins/bootstrap-typehead/typeahead.jquery.min.js') }}"></script>
<script src="{{ asset('template/pages/assets/plugins/handlebars/handlebars-v4.0.5.js') }}"></script>
<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="{{ asset('template/pages/pages/js/pages.min.js') }}"></script>
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL JS -->
<script src="{{ asset('template/pages/assets/js/form_elements.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/js/scripts.js') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL JS -->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js" type="text/javascript"></script>
<script src="http://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script>

$(document).ready(function() {
    $('#activated').DataTable({
      // dom: 'lBfrtip',
    });  

  });

  <?php 

    if(isset($data->status_log)){
      if($data->status_log == 1){
        echo "$('#log_active').prop('checked', 1)\n";
      } else {
        echo "$('#log_disactive').prop('checked', 1)\n";   
      }
    } 

    if(isset($data->status)){
      if($data->status == 1){
        echo "$('#status_active').prop('checked', 1)";
      } else {
        echo "$('#status_disactive').prop('checked', 1)";   
      }
    } 
  ?>
  

  // $('#log_active').prop('checked', 1)
  // $('#log_disactive').prop('checked', 1)
  // $('#status_active').prop('checked', 1)
  // $('#status_disactive').prop('checked', 1)  
</script>
@endpush