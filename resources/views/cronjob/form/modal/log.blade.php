 <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Log Cronjob</h4>

        </div>
        <div class="modal-body"><br>
          <!-- <p>This is a large modal.</p> -->
          <!-- <h4>Activated</h4> -->
          <div style="overflow-x:auto;">
          <table class="table" id="activated">
            <thead>
              <tr>
                <th>Message</th>
                <th>Status</th>
                <th>Last Sync</th>
              </tr>
            </thead>
            <tbody>              
                @foreach($log as $row)
                <tr>
                  <td>{{  str_replace('}', '', str_replace('{', '', $row->message)) }}</td>         
                  <td>{{$row->status or ''}}</td>
                  <td>{{$row->last_sync or ''}}</td>
                </tr>
                @endforeach              
            </tbody>
            
            
          </table>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>