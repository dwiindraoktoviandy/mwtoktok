@extends('layouts.main')

@push('style')
<link href="{{ asset('template/pages/assets/plugins/pace/pace-theme-flash.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/font-awesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/jquery-scrollbar/jquery.scrollbar.css') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset('template/pages/assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset('template/pages/assets/plugins/switchery/css/switchery.min.css') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset('template/pages/assets/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/bootstrap-tag/bootstrap-tagsinput.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/dropzone/css/dropzone.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/bootstrap-datepicker/css/datepicker3.css') }}" rel="stylesheet" type="text/css" media="screen">
<link href="{{ asset('template/pages/assets/plugins/summernote/css/summernote.css') }}" rel="stylesheet" type="text/css" media="screen">
<link href="{{ asset('template/pages/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet" type="text/css" media="screen">
<link href="{{ asset('template/pages/assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet" type="text/css" media="screen">
<link href="{{ asset('template/pages/pages/css/pages-icons.css') }}" rel="stylesheet" type="text/css">
<link class="main-stylesheet" href="{{ asset('template/pages/pages/css/pages.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')

<div class="jumbotron" data-pages="parallax">
  <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
    <div class="inner">
      <!-- START BREADCRUMB -->
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item"><a href="{{route("cronjob.index")}}">Cronjob</a></li>
        <li class="breadcrumb-item active">Setup Cronjob</li>
      </ol>
      <!-- END BREADCRUMB -->      
    </div>
  </div>
</div>

<!-- START CONTAINER FLUID -->
<div class=" container-fluid   container-fixed-lg">
  <div class="row">
    <div class="col-lg-6">
      <!-- START card -->
      <div class="card card-default">
        <div class="card-header ">
          <div class="card-title">
            Cronjob setup
          </div>
        </div>
        <div class="card-block">          
          <form class="" role="form" action="{{route("cronjob.store")}}" method="post">
            @csrf
            <div class="form-group form-group-default  ">
              <label>Vendor</label>
              <input type="text" name="vendor_id" class="form-control" disabled="">
            </div>            
            <div class="form-group form-group-default  ">
              <label>Api Name</label>
              <input type="text" name="name" class="form-control" disabled="">
            </div>            
            <div class="form-group form-group-default  ">
              <label>Api Url</label>
              <input type="text" name="url" class="form-control" disabled="">
            </div>            
            <div class="form-group form-group-default  ">
              <label>Menit</label>
              <select name="param_minute" class="form-control">              
                <option value="*"></option>
                @for($i=0;$i < 60; $i++)
                <option value="{{$i}}">{{$i}}</option>
                @endfor
              </select>              
            </div>            
            <div class="form-group form-group-default  ">
              <label>Jam</label>
              <select name="param_hour" class="form-control">              
                <option value="*"></option>
                @for($i=1;$i < 24; $i++)
                <option value="{{$i}}">{{$i}}</option>
                @endfor
              </select>
            </div>            
            <div class="form-group form-group-default  ">
              <label>Hari</label>
              <select name="param_day" class="form-control">              
                <option value="*"></option>
                @for($i=1;$i < 32; $i++)
                <option value="{{$i}}">{{$i}}</option>
                @endfor
              </select>
            </div>            
            <div class="form-group form-group-default  ">
              <label>Bulan</label>
              <select name="param_month" class="form-control">              
                <option value="*"></option>
                @for($i=1;$i < 13; $i++)
                <option value="{{$i}}">{{$i}}</option>
                @endfor
              </select>
            </div>            
            <button class="btn btn-complete btn-cons" type="submit">Submit</button>
          </form>
        </div>
      </div>
      <!-- END card -->
    </div>    
  </div>
</div>
<!-- END CONTAINER FLUID -->



@endsection

@push('scripts')
<!-- BEGIN VENDOR JS -->
<script src="{{ asset('template/pages/assets/plugins/pace/pace.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/modernizr.custom.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/tether/js/tether.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery/jquery-easy.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-unveil/jquery.unveil.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-ios-list/jquery.ioslist.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-actual/jquery.actual.min.js') }}"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/select2/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/classie/classie.js') }}"></script>
<script src="{{ asset('template/pages/assets/plugins/switchery/js/switchery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/jquery-autonumeric/autoNumeric.js') }}"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/dropzone/dropzone.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/bootstrap-tag/bootstrap-tagsinput.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/jquery-inputmask/jquery.inputmask.min.js') }}"></script>
<script src="{{ asset('template/pages/assets/plugins/bootstrap-form-wizard/js/jquery.bootstrap.wizard.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/summernote/js/summernote.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('template/pages/assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('template/pages/assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js') }}"></script>
<script src="{{ asset('template/pages/assets/plugins/bootstrap-typehead/typeahead.bundle.min.js') }}"></script>
<script src="{{ asset('template/pages/assets/plugins/bootstrap-typehead/typeahead.jquery.min.js') }}"></script>
<script src="{{ asset('template/pages/assets/plugins/handlebars/handlebars-v4.0.5.js') }}"></script>
<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="{{ asset('template/pages/pages/js/pages.min.js') }}"></script>
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL JS -->
<script src="{{ asset('template/pages/assets/js/form_elements.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/js/scripts.js') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL JS -->
@endpush