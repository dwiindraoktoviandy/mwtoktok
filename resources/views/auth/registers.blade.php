@extends('layouts.main')

@push('style')
<link href="{{ asset('template/pages/assets/plugins/pace/pace-theme-flash.css ') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/bootstrap/css/bootstrap.min.css ') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/font-awesome/css/font-awesome.css ') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/jquery-scrollbar/jquery.scrollbar.css ') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset('template/pages/assets/plugins/select2/css/select2.min.css ') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset('template/pages/assets/plugins/switchery/css/switchery.min.css ') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset('template/pages/assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css ') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css ') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/datatables-responsive/css/datatables.responsive.css ') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset('template/pages/pages/css/pages-icons.css ') }}" rel="stylesheet" type="text/css">
<link class="main-stylesheet" href="{{ asset('template/pages/pages/css/pages.css ') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
.tengah {
    text-align: center;
}

.notbold {
    font-weight: 500;
}
</style>
@endpush

@section('content')
<div class="jumbotron" data-pages="parallax">
    <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
        <div class="inner">
            <!-- START BREADCRUMB -->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">ACL</li>
                <li class="breadcrumb-item">User</li>
                <li class="breadcrumb-item active">Daftar Pengguna</li>
            </ol>
            <!-- END BREADCRUMB -->
        </div>
    </div>
</div>
<!-- START CONTAINER FLUID -->
<div class=" container-fluid container-fixed-lg bg-white">
    <!-- START card -->
    <div class="card card-transparent">
        <div class="col-sm-12" style="text-align: center;"><br><H3>Daftar Pengguna</H3></div>
<!--         <div class="card-header d-flex justify-content-between">
          <div class="row">
            <a class="btn btn-complete btn-cons" href="#">Get Stock Vendor</a>
            <a class="btn btn-danger btn-cons" href="#">Delete Stock Product</a>
          </div>
        </div> -->

        <!-- isi -->


                 <text class="alert-msg"></text>
    
      <!-- &nbsp;&nbsp;&nbsp;<a class="btn btn-primary" href="#">Add New</a>&nbsp; -->
              @if(session()->has('message.level'))
                <div class="alert alert-{{ session('message.level') }}"> 
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <font color="#0a7c71">{!! session('message.content') !!}</font>
                </div>
              @endif
        <div class="col-md-12">
               <!-- START card -->


  <div class="col-xl-6 col-lg-6 ">
    <!-- START card -->
    <div class="card card-transparent">
      <div class="card-block">
        {!!$data ? '<form id="form-personal" role="form" autocomplete="off" method="POST" action="'.route('user.update', [$data->id]).'">'. method_field('PUT') : '<form id="form-personal" role="form" autocomplete="off" method="POST" action="'.route('user.store').'">'!!}
        
        {{ csrf_field() }}
          <div class="row">
            <div class="col-md-12">
              <div class="form-group form-group-default">
                <label>Nama</label>
                <input id="name" type="text" class="form-control" name="name" value="{{$data ? $data->name : old('name')}}" required autofocus>
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group form-group-default">
                <label>Nama Pengguna</label>
                <input id="username" type="text" class="form-control" name="username" value="{{$data ? $data->username : old('username')}}" required autofocus>
                @if ($errors->has('username'))
                    <span class="help-block">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                @endif
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group form-group-default">
                <label>Email</label>
                <input id="email" type="email" class="form-control" name="email" value="{{$data ? $data->email : old('email')}}" required>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
              </div>
            </div>
          </div>
          <div class="row clearfix">
            <div class="col-md-6">
              <div class="form-group form-group-default ">
                <label>Kata Sandi</label>                
                <input id="password" type="password" class="form-control" name="password" >
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-group-default">
                <label>Konfirmasi Kata Sandi</label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" >
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group form-group-default form-group-default-select2 required">
                <label class="">Grup Pengguna</label>
                <select class="full-width" data-placeholder="Select Country" data-init-plugin="select2" name="usergroup_id">
                  {{$data ? '<option value="'.$data->usergroup_id.'">'.$data->usergroup->name.'</option>' : '<option value=""></option>'}}
                   @if(\App\Models\UserGroup::count() > 0)
                    @foreach(\App\Models\UserGroup::orderBy('name')->get() as $row)
                      <option value="{{ $row->id }}" {{ !empty($data->usergroup_id) ? $data->usergroup_id == $row->id ? 'selected="selected"' : '' : '' }}>{{ $row->name }}</option>      
                    @endforeach
                  @endif
                </select>
              </div>
            </div>
          </div>
                    
          <p class="pull-left">
            <!-- I agree to the <a href="#">Pages Terms</a> and <a href="#">Privacy</a>. -->
          </p>
          <p class="pull-right">
            <!-- <a href="#">Help? Contact Support</a> -->
          </p>
          <div class="clearfix"></div>
          {!!$data ? '<button class="btn btn-complete" type="submit">Perbaharui akun</button>&nbsp;<a class="btn btn-complete" href="'.route("user.index").'">Cancel</a>' : '<button class="btn btn-complete" type="submit">Buat akun baru</button>'!!}
          
        </form>
      </div>
    </div>
    <!-- END card -->
  </div>
  <div class="col-xl-12 col-lg-12 bg-white">
    <!-- START card -->
    <div class="card card-transparent">
      <div class="card-header ">
        <div class="card-title">Daftar Pengguna
        </div>
        <div class="pull-right">
          <div class="col-xs-12">
            <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="card-block bg-white" style="overflow-x:auto;">
        <table class="table table-hover demo-table-search table-responsive-block display responsive no-wrap" id="tableData" width="100%">
          <thead>
            <tr>
              <th>#</th>
              <th>Nama</th>
              <th>Email</th>
              <th>Nama Pengguna</th>
              <th>Created At</th>
              <th>Pembaharuan Terakhir</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            
          </tbody>
        </table>
      </div>
    </div>
    <!-- END card -->
  </div>



        <!-- endisi -->

    </div>
    <!-- END card -->
</div>
<!-- END CONTAINER FLUID -->
@endsection

@push('scripts')
<!-- BEGIN VENDOR JS -->
<script src="{{ asset('template/pages/assets/plugins/pace/pace.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery/jquery-1.11.1.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/modernizr.custom.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-ui/jquery-ui.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/tether/js/tether.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/bootstrap/js/bootstrap.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery/jquery-easy.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-unveil/jquery.unveil.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-ios-list/jquery.ioslist.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-actual/jquery.actual.min.js ') }}"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js ') }}"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/select2/js/select2.full.min.js ') }}"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/classie/classie.js ') }}"></script>
<script src="{{ asset('template/pages/assets/plugins/switchery/js/switchery.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js ') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/datatables-responsive/js/datatables.responsive.js ') }}"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/datatables-responsive/js/lodash.min.js ') }}"></script>
<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="{{ asset('template/pages/pages/js/pages.min.js ') }}"></script>
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL JS -->
<script src="{{ asset('template/pages/assets/js/datatables.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/js/scripts.js ') }}" type="text/javascript"></script>
<script>

  // var app = new Vue({
  //   el: '#app',
  //   data: {
  //     message: ''
  //   },
  //   methods:{
  //     say: function (argument) {
  //       // body...
  //       alert(argument)
  //     }
  //   }
  // })



  var table = $('#tableData');

  var settings = {
      "sDom": "<t><'row'<p i>>",
      // "destroy": true,
      "responsive": true,
      "scrollCollapse": true,
      "oLanguage": {
          "sLengthMenu": "_MENU_ ",
          "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
      },
      "iDisplayLength": 6,
      columns: [        
        {data :'id'},
        {data :'name'},
        {data :'email'},
        {data :'username'},
        {data :'created_at'},
        {data :'updated_at'},        
        {data :'action'},
      ],
      "columnDefs": [
          {
              "targets": [ 0 ],
              "visible": false,
              "searchable": false
          }          
      ],
      "order": [[4, "desc"]]
  };

  table.DataTable(settings);
  $('#search-table').keyup(function() {
      table.fnFilter($(this).val());
  });

  table.DataTable().ajax.url( '{{route("user.user-list")}}' ).load();  
    
</script>
<!-- END PAGE LEVEL JS -->
@endpush