@extends('layouts.main')

@push('style')
<link href="{{ asset('template/pages/assets/plugins/pace/pace-theme-flash.css ') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/bootstrap/css/bootstrap.min.css ') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/font-awesome/css/font-awesome.css ') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/jquery-scrollbar/jquery.scrollbar.css ') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset('template/pages/assets/plugins/select2/css/select2.min.css ') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset('template/pages/assets/plugins/switchery/css/switchery.min.css ') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset('template/pages/assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css ') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css ') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('template/pages/assets/plugins/datatables-responsive/css/datatables.responsive.css ') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset('template/pages/pages/css/pages-icons.css ') }}" rel="stylesheet" type="text/css">
<link class="main-stylesheet" href="{{ asset('template/pages/pages/css/pages.css ') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
.tengah {
    text-align: center;
}

.notbold {
    font-weight: 500;
}
</style>
@endpush

@section('content')
<div class="jumbotron" data-pages="parallax">
    <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
        <div class="inner">
            <!-- START BREADCRUMB -->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Beranda</li>
                <!-- <li class="breadcrumb-item">Transaction</li> -->
                <li class="breadcrumb-item active">Daftar Pengguna</li>
            </ol>
            <!-- END BREADCRUMB -->
        </div>
    </div>
</div>
<!-- START CONTAINER FLUID -->
<div class=" container-fluid container-fixed-lg bg-white">
    <!-- START card -->
    <div class="card card-transparent">
        <div class="col-sm-12" style="text-align: center;"><br><H3>Daftar Grup Pengguna</H3></div>
<!--         <div class="card-header d-flex justify-content-between">
          <div class="row">
            <a class="btn btn-complete btn-cons" href="#">Get Stock Vendor</a>
            <a class="btn btn-danger btn-cons" href="#">Delete Stock Product</a>
          </div>
        </div> -->
<!-- isi -->

        <div class="row">
  <div class="col-xl-5 col-lg-6 ">
    <!-- START card -->
    <div class="card card-default">
      <div class="card-block">
        {!!$data ? '<form id="form-personal" role="form" autocomplete="off" method="POST" action="'.route('usergroups.update', [$data->id]).'">'. method_field('PUT') : '<form id="form-personal" role="form" autocomplete="off" method="POST" action="'.route('usergroups.store').'">'!!}
        
        {{ csrf_field() }}
          <div class="row">
            <div class="col-md-12">
              <div class="form-group form-group-default">
                <label>Nama</label>
                <input id="name" type="text" class="form-control" name="name" value="{{$data ? $data->name : old('name')}}" required autofocus>
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group form-group-default">
                <label>Deskripsi</label>
                <textarea name="description" class="form-control"  rows="20">{{$data ? $data->description : old('description')}}</textarea>                
                @if ($errors->has('description'))
                    <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif
              </div>
            </div>
          </div>        
          <div class="row">
            <div class="col-md-12">
              <div data-pages="card" class="card card-default" id="card-basic">
                <div class="card-header  ">
                  <div class="card-title"> <h4>
                  <span class="semi-bold">Route</span> Akses</h4>
                  </div>
                  <div class="card-controls">
                    <ul>
                      <li><a data-toggle="collapse" class="card-collapse" href="#"><i
                class="card-icon card-icon-collapse"></i></a>
                      </li>                      
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="card-block">                  
                  <p><?php 
                            $routeCollection = Route::getRoutes();
                            $new             = [];
                            $groups          = [];
                            foreach ($routeCollection as $value) {

                                if(($value->getName() != '') or (!$value)){
                                    if(strpos($value->getName(), 'front-user') === false){
                                        
                                        if (in_array(explode(".",$value->getName())[0], $groups)) {
                                            
                                        } else {

                                            array_push($groups, explode(".",$value->getName())[0]);
                                        }                            
                                        array_push($new, $value->getName());
                                    }
                                }                            
                            } 
                            asort($groups);
                            foreach ($groups as $key => $group) {
                                # code...
                                echo '<label class="switch"><input type="checkbox" class="skip" onclick="checkclass(\''.$group.'\')" id="'.$group.'"/><span></span><em> '.$group.'</em> </label></br>';
                                foreach ($routeCollection as $row) {
                                    if(explode(".",$row->getName())[0] == $group){

                                        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class="switch"><input type="checkbox" name="routelist[]" class="'.$group.' skip" value="'.$row->getName().'"/><span></span> '.$row->getName().'</label></br>';
                                    }
                                }
                                echo '</br>';
                            }
                            
                        ?>
                  </p>                  
                </div>
              </div>
            </div>
          </div>         
          
          <div class="clearfix"></div>
          {!!$data ? '<button class="btn btn-complete" type="submit">Perbarui grup</button>&nbsp;<a class="btn btn-complete" href="'.route("usergroups.index").'">Cancel</a>' : '<button class="btn btn-complete" type="submit">Buat grup baru</button>'!!}
          
        </form>
      </div>
    </div>
    <!-- END card -->
  </div>
  <div class="col-xl-7 col-lg-6 bg-white">
    <!-- START card -->
    <div class="card card-transparent">
      <div class="card-header ">
        <div class="card-title">Daftar Grup Pengguna
        </div>
        <div class="pull-right">
          <div class="col-xs-12">
            <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="card-block bg-white">
        <table class="table table-hover demo-table-search table-responsive-block" id="tableData">
          <thead>
            <tr>
              <th>#</th>
              <th>Nama</th>
              <th>Deskripsi</th>              
              <th>Dibuat pada</th>
              <th>Pembaharuan Terakhir</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            
          </tbody>
        </table>
      </div>
    </div>
    <!-- END card -->
  </div>
</div>

<!-- endisi -->
        </div>
    </div>
    <!-- END card -->
</div>
<!-- END CONTAINER FLUID -->
@endsection

@push('scripts')
<!-- BEGIN VENDOR JS -->
<script src="{{ asset('template/pages/assets/plugins/pace/pace.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery/jquery-1.11.1.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/modernizr.custom.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-ui/jquery-ui.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/tether/js/tether.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/bootstrap/js/bootstrap.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery/jquery-easy.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-unveil/jquery.unveil.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-ios-list/jquery.ioslist.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-actual/jquery.actual.min.js ') }}"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js ') }}"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/select2/js/select2.full.min.js ') }}"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/classie/classie.js ') }}"></script>
<script src="{{ asset('template/pages/assets/plugins/switchery/js/switchery.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js ') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/datatables-responsive/js/datatables.responsive.js ') }}"></script>
<script type="text/javascript" src="{{ asset('template/pages/assets/plugins/datatables-responsive/js/lodash.min.js ') }}"></script>
<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="{{ asset('template/pages/pages/js/pages.min.js ') }}"></script>
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL JS -->
<script src="{{ asset('template/pages/assets/js/datatables.js ') }}" type="text/javascript"></script>
<script src="{{ asset('template/pages/assets/js/scripts.js ') }}" type="text/javascript"></script>
<script>

  // var app = new Vue({
  //   el: '#app',
  //   data: {
  //     message: ''
  //   },
  //   methods:{
  //     say: function (argument) {
  //       // body...
  //       alert(argument)
  //     }
  //   }
  // })



 var table = $('#tableData');

  var settings = {
      "sDom": "<t><'row'<p i>>",
      // "destroy": true,
      "scrollCollapse": true,
      "oLanguage": {
          "sLengthMenu": "_MENU_ ",
          "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
      },
      "iDisplayLength": 6,
      columns: [        
        {data :'id'},
        {data :'name'},
        {data :'description'},        
        {data :'created_at'},
        {data :'updated_at'},        
        {data :'action'},
      ],
      "columnDefs": [
          {
              "targets": [ 0 ],
              "visible": false,
              "searchable": false
          }          
      ]

  };

  table.DataTable(settings);
  $('#search-table').keyup(function() {
      table.fnFilter($(this).val());
  });

  table.DataTable().ajax.url( '{{route("usergroups.usergroups-list")}}' ).load();

  function checkclass(data)
  {   
      if($( "#"+data ).is(':checked'))
      {                    
          $( "."+data ).not(this).prop( "checked", true );        
      } 
      else 
      {                    
          $( "."+data ).not(this).prop( "checked", false );        
      }        

  }

  $("#login").not(this).prop( "checked", true ).attr('readonly')
  $(".login").not(this).prop( "checked", true ).attr('readonly')
  $("#logout").not(this).prop( "checked", true ).attr('readonly')
  $(".logout").not(this).prop( "checked", true ).attr('readonly')  
  $("#index").not(this).prop( "checked", true ).attr('readonly')
  $(".index").not(this).prop( "checked", true ).attr('readonly')

  @if($data)
    @foreach (json_decode($data->access_route)->routelist as $listcheck)
      $( '[value="{{$listcheck}}"]' ).not(this).prop( "checked", true );              
    @endforeach
  @endif


    
</script>
<!-- END PAGE LEVEL JS -->
@endpush